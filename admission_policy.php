<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Admission Policy – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Admission Policy</h1>
				<h5>Admissions</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="why_ubit.php" class="list-group-item grey lighten-4">Why Choose UBIT</a>
						<a href="affiliated_colleges.php" class="list-group-item grey lighten-4">Affiliated Colleges</a>
						<a href="fee_structure.php" class="list-group-item grey lighten-4">Fees Structure</a>
						<a href="admission_policy.php" class="list-group-item active">Admission Policy</a>
						<a href="eligibility_criteria.php" class="list-group-item grey lighten-4">Eligibility Criteria</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color">Policy Statement</h4>
					<p>UBIT welcomes all applicants qualified to achieve the University’s laid down academic eligibility standards without regard to gender, race, creed, or age. Two percent seats are reserved for disable persons.</p>
					
					<h4 class="green-color mt-4">Admission Merit</h4>
					<p>Merit for admissions is announced on the basis of the entry test results,(or NTS/ETS Score) , academic eligibility qualification marks and interview. Admission / Enrollment in any program is limited to the number of seats who can be accommodated.</p>
					
					<h4 class="green-color mt-4">Students Dropped from UBIT</h4>
					<p>Students once dropped from UBIT on academic grounds will not be admitted to UBIT subsequently in the same program. Those dropped on disciplinary grounds are not eligible for re-admission in any program.</p>
					
					<h4 class="green-color mt-4">How to Apply</h4>
					<p>Admissions to UBIT are advertised in the leading national dailies and on the same website, in the month of June/July and Nov/Dec every year for Fall Semester except for Engineering programs which are advertised in June / July for Fall Semester (only one intake per year). Prospective students are required to fill the online application form and apply for the program before the closing date.</p>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js">
	</script>
	<script type="text/javascript" src="js/popper.min.js">
	</script>
	<script type="text/javascript" src="js/bootstrap.min.js">
	</script>
	<script type="text/javascript" src="js/mdb.min.js">
	</script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>