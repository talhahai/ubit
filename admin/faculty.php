<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['login']))
{
	header('location: login.php');
}

if (!isset($_GET['id']))
{
	header('location: faculties.php');
}	

require_once 'functions.php';

$Data = Search_Query("SELECT * from faculty where id = '".$_GET['id']."'")[0];
$Educations = Search_Query("SELECT * from educations where facultyid = '".$_GET['id']."' order by priority");
$Interests = Search_Query("SELECT * from interests where facultyid = '".$_GET['id']."' order by priority");
$Awards = Search_Query("SELECT * from awards where facultyid = '".$_GET['id']."' order by priority");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="...images/favicon.ico" type="image/x-icon">
	<title><?php echo $Data['Name'] ?> – Admin Panel – UBIT</title>
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/mdb.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2"><?php echo $Data['Name'] ?></h1>
				<h5>Faculty</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<h4 class="green-color my-4">Faculty Data</h4>
			<div class="card">
				<div class="card-body">
					<form method="post" id="form-update-data">
						<div class="row">
							<div class="col-md-6">
								<div class="md-form">
									<div class="md-form">
										<input type="text" name="name" id="name" class="form-control" value="<?php echo $Data['Name'] ?>" required>
										<label for="name" class="">Name</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="md-form">
									<div class="md-form">
										<input type="text" name="designation" id="designation" class="form-control" value="<?php echo $Data['Designation'] ?>" required>
										<label for="designation" class="">Designation</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="md-form">
									<div class="md-form">
										<input type="text" name="degrees" id="degrees" class="form-control" value="<?php echo $Data['Degrees'] ?>">
										<label for="degrees" class="">Degrees</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="md-form">
									<div class="md-form">
										<input type="text" name="email" id="email" class="form-control" value="<?php echo $Data['Email'] ?>">
										<label for="email" class="">Email</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row mb-4 mb-md-0">
							<div class="col-md-6">
								<div class="md-form">
									<div class="md-form">
										<input type="text" name="quote" id="quote" class="form-control" value="<?php echo $Data['Quote'] ?>">
										<label for="quote" class="">Quote</label>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<label>Image</label>
								<div class="md-form">
									<input type="file" id="image" name="image" class="formcontrol">
								</div>
							</div>
							<div class="col-md-3">
								<img src="../<?php echo $Data['Image'] ?>" class="img-fluid">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="md-form">
									<textarea type="text" name="biography" id="biography" class="md-textarea" rows="10" style="height: auto;"><?php echo $Data['Biography'] ?></textarea>
									<label for="biography">Biography</label>
								</div>
							</div>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-rounded btn-outline-black waves-effect" id="update-data">UPDATE DATA</button>
						</div>
					</form>
				</div>
			</div>

			<div class="row mt-4">
				<div class="col-6 align-self-center">
					<h4 class="green-color my-4">Educations</h4>
				</div>
				<div class="col-6 pull-right align-self-center">
					<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit" id="btnAddEducation">ADD EDUCATION</a>
				</div>
			</div>
			<div class="card">
				<table class="table">
					<thead>
						<tr>
							<th class="font-weight-bold">Description</th>
							<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
							<th class="text-center font-weight-bold" style="width: 50px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($Educations as $Education)
						{
							?>
							<tr>
								<td><?php echo $Education['Description'] ?></td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="educations" data-action="up" data-id="<?php echo $Education['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="educations" data-action="down" data-id="<?php echo $Education['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
								</td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light" data-entity="educations" data-id="<?php echo $Education['ID'] ?>"><i class="fa fa-remove"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

			<div class="row mt-4">
				<div class="col-6 align-self-center">
					<h4 class="green-color my-4">Awards</h4>
				</div>
				<div class="col-6 pull-right align-self-center">
					<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit" id="btnAddAward">ADD AWARD</a>
				</div>
			</div>
			<div class="card">
				<table class="table">
					<thead>
						<tr>
							<th class="font-weight-bold">Description</th>
							<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
							<th class="text-center font-weight-bold" style="width: 50px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($Awards as $Award)
						{
							?>
							<tr>
								<td><?php echo $Award['Description'] ?></td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="awards" data-action="up" data-id="<?php echo $Award['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="awards" data-action="down" data-id="<?php echo $Award['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
								</td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light" data-entity="awards" data-id="<?php echo $Award['ID'] ?>"><i class="fa fa-remove"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

			<div class="row mt-4">
				<div class="col-6 align-self-center">
					<h4 class="green-color my-4">Interests</h4>
				</div>
				<div class="col-6 pull-right align-self-center">
					<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAddEdit" id="btnAddInterest">ADD INTEREST</a>
				</div>
			</div>
			<div class="card">
				<table class="table">
					<thead>
						<tr>
							<th class="font-weight-bold">Description</th>
							<th class="text-center font-weight-bold" style="width: 100px">Priority</th>
							<th class="text-center font-weight-bold" style="width: 50px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($Interests as $Interest)
						{
							?>
							<tr>
								<td><?php echo $Interest['Description'] ?></td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="interests" data-action="up" data-id="<?php echo $Interest['ID'] ?>"><i class="fa fa-arrow-up"></i></a>
									<a class="m-0 btn-sm btn-floating unique-color waves-effect waves-light btn-priority" data-entity="interests" data-action="down" data-id="<?php echo $Interest['ID'] ?>"><i class="fa fa-arrow-down"></i></a>
								</td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light" data-entity="interests" data-id="<?php echo $Interest['ID'] ?>"><i class="fa fa-remove"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

			<div class="modal fade" id="modalAddEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
					<div class="modal-content">
						<div class="modal-header light-blue darken-3 white-text">
							<h4 class="title"><i class="fa fa-plus"></i> Add <span class="headingEntity">Education</span></h4>
							<button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<form method="post" id="form-entity">
							<div class="modal-body mb-0">
								<input type="hidden" id="inputID" name="id" value="0">
								<input type="hidden" id="inputEntity" name="entity" value="education">
								<input type="hidden" id="inputAction" name="action" value="add">
								<div class="md-form">
									<input type="text" id="description" name="description" class="form-control" required>
									<label for="description">Description</label>
								</div>
							</div>
							<div class="modal-footer d-flex justify-content-center">
								<button type="submit" class="btn btn-primary" id="add-entity">ADD <span class="headingEntity">Education</span></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script type="text/javascript" src="../js/bootbox.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			<?php 
			if (isset($_SESSION['toast-message']))
			{
				echo "toastr.success('".$_SESSION['toast-message']."', '', {positionClass: 'toast-bottom-left'});";
				unset($_SESSION['toast-message']);
			}
			?>

			$("#form-update-data").submit(function(e) {
				e.preventDefault();
				$("#update-data").prop('disabled', 'disabled')
				$("#update-data").html('<i class="fa fa-spinner fa-spin mr-2"></i> UPDATING DATA');
				var formData = new FormData(this);
				formData.append('id', <?php echo $_GET['id'] ?>);
				$.ajax({
					type: "POST",
					url: "editfaculty.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Faculty edited successfully', '', {positionClass: 'toast-bottom-left'});
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#update-data").prop('disabled', '')
						$("#update-data").html('UPDATE DATA');
					}
				});
			});

			$(".btn-priority").click(function() {
				var SelectedItem = $(this);
				SelectedItem.prop('disabled', 'disabled')
				SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
				$.ajax({
					type: "get",
					url: "updatepriority.php",
					data: "id="+SelectedItem.data('id')+"&action="+SelectedItem.data('action')+"&entity="+SelectedItem.data('entity'),
					success: function (data) {
						if (data == 'true')
						{
							location.reload();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						SelectedItem.prop('disabled', '')
						SelectedItem.html('<i class="fa fa-remove"></i>');
					}
				});
			});

			$(".btn-delete").click(function() {
				var SelectedItem = $(this);
				bootbox.confirm({
					message: "Do you want to really wish to delete this "+SelectedItem.data('entity')+"?",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'danger-color-dark'
						},
						cancel: {
							label: 'No',
							className: 'btn-green'
						}
					},
					callback: function (result) {
						if (result == true)
						{
							SelectedItem.prop('disabled', 'disabled')
							SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
							$.ajax({
								type: "get",
								url: "deleteentity.php",
								data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity'),
								success: function (data) {
									if (data == 'true')
									{
										location.reload();
									}
									else
									{
										toastr.error(data, '', {positionClass: 'toast-bottom-left'});
									}
									SelectedItem.prop('disabled', '')
									SelectedItem.html('<i class="fa fa-remove"></i>');
								}
							});
						}
					}
				});
			});

			$("#btnAddEducation").click(function() {
				$(".headingEntity").html("Education");
				$("#inputEntity").val("educations");
				$("#inputAction").val("add");
			});

			$("#btnAddAward").click(function() {
				$(".headingEntity").html("Award");
				$("#inputEntity").val("awards");
				$("#inputAction").val("add");
			});

			$("#btnAddInterest").click(function() {
				$(".headingEntity").html("Interest");
				$("#inputEntity").val("interests");
				$("#inputAction").val("add");
			});

			$("#form-entity").submit(function(e) {
				e.preventDefault();
				$("#add-entity").prop('disabled', 'disabled')
				$("#add-entity").html('<i class="fa fa-spinner fa-spin mr-2"></i> ADDING <span class="headingEntity">'+$(".headingEntity").html()+'</span>');
				var formData = new FormData(this);
				formData.append('facultyid', <?php echo $_GET['id'] ?>);
				$.ajax({
					type: "POST",
					url: formData.get('action')+"entity.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success($(".headingEntity").html()+' '+formData.get('action')+'ed successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-entity")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#add-entity").prop('disabled', '')
						$("#add-entity").html('ADD <span class="headingEntity">'+$(".headingEntity").html()+'</span>');
					}
				});
			});
		});
	</script>
</body>
</html>