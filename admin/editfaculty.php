<?php

require_once 'functions.php';

if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['designation']) && isset($_POST['quote']) && isset($_POST['biography']) && isset($_POST['degrees']) && isset($_POST['email']))
{
	if (!empty($_POST['id']) && !empty($_POST['name']) && !empty($_POST['designation']))
	{
		if(mysqli_query($Connection, "UPDATE faculty set
			name = '".mysqli_real_escape_string($Connection, $_POST['name'])."',
			designation = '".mysqli_real_escape_string($Connection, $_POST['designation'])."',
			quote = '".mysqli_real_escape_string($Connection, $_POST['quote'])."',
			degrees = '".mysqli_real_escape_string($Connection, $_POST['degrees'])."',
			email = '".mysqli_real_escape_string($Connection, $_POST['email'])."',
			biography = '".mysqli_real_escape_string($Connection, $_POST['biography'])."'
			where id = '".$_POST['id']."'"))
		{
			if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"]))
			{
				$FileExtension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

				if ($FileExtension != 'png' && $FileExtension != 'jpg' && 
					$FileExtension != 'jpeg' && $FileExtension != 'zip')
				{
					echo "File format not suported";
					return;
				}

				$CurrentImage = Search_Query("SELECT Image from faculty where id = '".$_POST['id']."'")[0]['Image'];
				if ($CurrentImage != 'images/faculty/na-male.png' && $CurrentImage != 'images/faculty/na-female.png')
					unlink('../'.$CurrentImage);

				$FileName = 'images/faculty/'.$_POST['id'].'.'.$FileExtension;

				if (move_uploaded_file($_FILES['image']['tmp_name'], '../'.$FileName))
				{
					mysqli_query($Connection, "UPDATE faculty set Image = '".$FileName."' where id = '".$_POST['id']."'");
				}
			}

			echo 'true';
			return;
		}
		else
		{	
			echo "Faculty not edited, please try again later";
			return;
		}
	}
}

echo "Faculty not edited, try to fill required fields";
return;

?>