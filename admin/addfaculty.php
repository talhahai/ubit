<?php

require_once 'functions.php';

if (isset($_POST['name']) && isset($_POST['designation']))
{
	if (!empty($_POST['name']) && !empty($_POST['designation']))
	{
		if ($_POST['gender'] == 'male')
			$FileName = 'images/faculty/na-male.png';
		else
			$FileName = 'images/faculty/na-female.png';

		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from faculty")[0]['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO faculty set
			name = '".mysqli_real_escape_string($Connection, $_POST['name'])."',
			designation = '".mysqli_real_escape_string($Connection, $_POST['designation'])."',
			Image = '".$FileName."',
			Priority = '".$MaxPriority."'"))
		{
			$FacultyID = mysqli_insert_id($Connection);

			if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"]))
			{
				$FileExtension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

				if ($FileExtension != 'png' && $FileExtension != 'jpg' && 
					$FileExtension != 'jpeg' && $FileExtension != 'zip')
				{
					echo "File format not suported";
					return;
				}

				$FileName = 'images/faculty/'.$FacultyID.'.'.$FileExtension;

				if (move_uploaded_file($_FILES['image']['tmp_name'], '../'.$FileName))
				{
					mysqli_query($Connection, "UPDATE faculty set Image = '".$FileName."' where id = '".$FacultyID."'");
				}
			}

			echo 'true';
			return;
		}
		else
		{	
			echo "Faculty not added, please try again later";
			return;
		}
	}
}

echo "Faculty not added, try to fill required fields";
return;

?>