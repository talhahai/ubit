<?php

$HTML = file_get_contents("http://www.dcsubit.com/faculty/mr-muhammad-sajid/");
$HTML = substr($HTML, 0, strpos($HTML, '<div class="staff-sidebar fourcol last">'));
$HTML = substr($HTML, strpos($HTML, '"staff-title entry-title">') + 26);
$Name = substr($HTML, 0, strpos($HTML, '</h1>'));

$HTML = substr($HTML, strpos($HTML, 'post-description"><p>') + 21);
$Designation = substr($HTML, 0, strpos($HTML, '</p>'));

$HTML = substr($HTML, strpos($HTML, 'div class="staff-content eightcol">') + 35);

$HTML = substr($HTML, strpos($HTML, '<p>') + 3);
$Bio = substr($HTML, 0, strpos($HTML, '<h3>'));

$Section1Name = null;
$Section2Name = null;
$Section3Name = null;
$Section1Items = null;
$Section2Items = null;
$Section3Items = null;

if (strpos($HTML, '<h3>') !== false)
{
	$HTML = substr($HTML, strpos($HTML, '<h3>') + 4);
	$Section1Name = substr($HTML, 0, strpos($HTML, '</h3>'));
	$Section1 = substr($HTML, strpos($HTML, '<ul>') + 4);
	$Section1Items = substr($Section1, 0, strpos($Section1, '</ul>'));
	$Section1Items = str_replace('<li>', '', $Section1Items);
	$Section1Items = explode('</li>', $Section1Items);
	foreach ($Section1Items as $Key => $Item)
	{
		$Section1Items[$Key] = trim($Item);
	}
	$Section1Items = implode($Section1Items, '~~~~');
	$HTML = substr($HTML, strpos($HTML, '</ul>') + 4);
}

if (strpos($HTML, '<h3>') !== false)
{
	$HTML = substr($HTML, strpos($HTML, '<h3>') + 4);
	$Section2Name = substr($HTML, 0, strpos($HTML, '</h3>'));
	$Section2 = substr($HTML, strpos($HTML, '<ul>') + 4);
	$Section2Items = substr($Section2, 0, strpos($Section2, '</ul>'));
	$Section2Items = str_replace('<li>', '', $Section2Items);
	$Section2Items = explode('</li>', $Section2Items);
	$HTML = substr($HTML, strpos($HTML, '</ul>') + 4);
	foreach ($Section2Items as $Key => $Item)
	{
		$Section2Items[$Key] = trim($Item);
	}
	$Section2Items = implode($Section2Items, '~~~~');
}

if (strpos($HTML, '<h3>') !== false)
{
	$HTML = substr($HTML, strpos($HTML, '<h3>') + 4);
	$Section3Name = substr($HTML, 0, strpos($HTML, '</h3>'));
	$Section3 = substr($HTML, strpos($HTML, '<ul>') + 4);
	$Section3Items = substr($Section3, 0, strpos($Section3, '</ul>'));
	$Section3Items = str_replace('<li>', '', $Section3Items);
	$Section3Items = explode('</li>', $Section3Items);
	foreach ($Section3Items as $Key => $Item)
	{
		$Section3Items[$Key] = trim($Item);
	}
	$Section3Items = implode($Section3Items, '~~~~');
	$HTML = substr($HTML, strpos($HTML, '</ul>') + 4);
}

echo $Name;
echo '<br>';
echo $Designation;
echo '<br>';
echo $Bio;
echo '<br>';
echo $Section1Name;
echo '<br>';
echo $Section1Items;
echo '<br>';
echo $Section2Name;
echo '<br>';
echo $Section2Items;
echo '<br>';
echo $Section3Name;
echo '<br>';
echo $Section3Items;
echo '<br>';

// require_once 'functions.php';
// $MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from faculty")[0]['MaxPriority'] + 1;
// mysqli_query($Connection, "INSERT INTO faculty set
// 	name = '".mysqli_real_escape_string($Connection, $Name)."',
// 	designation = '".mysqli_real_escape_string($Connection, $Designation)."',
// 	Image = 'images/faculty/na-male.png',
// 	Priority = '".$MaxPriority."'");
// echo "INSERT INTO faculty set
// 	name = '".mysqli_real_escape_string($Connection, $Name)."',
// 	designation = '".mysqli_real_escape_string($Connection, $Designation)."',
// 	Image = 'images/faculty/na-male.png',
//	Priority = '".$MaxPriority."'";

die();


if (isset($_POST['name']) && isset($_POST['designation']))
{
	if (!empty($_POST['name']) && !empty($_POST['designation']))
	{
		if ($_POST['gender'] == 'male')
			$FileName = 'images/faculty/na-male.png';
		else
			$FileName = 'images/faculty/na-female.png';

		$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from faculty")[0]['MaxPriority'] + 1;
		if(mysqli_query($Connection, "INSERT INTO faculty set
			name = '".mysqli_real_escape_string($Connection, $_POST['name'])."',
			designation = '".mysqli_real_escape_string($Connection, $_POST['designation'])."',
			Image = '".$FileName."',
			Priority = '".$MaxPriority."'"))
		{
			$FacultyID = mysqli_insert_id($Connection);

			if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"]))
			{
				$FileExtension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

				if ($FileExtension != 'png' && $FileExtension != 'jpg' && 
					$FileExtension != 'jpeg' && $FileExtension != 'zip')
				{
					echo "File format not suported";
					return;
				}

				$FileName = 'images/faculty/'.$FacultyID.'.'.$FileExtension;

				if (move_uploaded_file($_FILES['image']['tmp_name'], '../'.$FileName))
				{
					mysqli_query($Connection, "UPDATE faculty set Image = '".$FileName."' where id = '".$FacultyID."'");
				}
			}

			echo 'true';
			return;
		}
		else
		{	
			echo "Faculty not added, please try again later";
			return;
		}
	}
}

echo "Faculty not added, try to fill required fields";
return;

?>