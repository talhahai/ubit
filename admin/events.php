<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['login']))
{
	header('location: login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="...images/favicon.ico" type="image/x-icon">
	<title>Events – Admin Panel – UBIT</title>
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/mdb.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Events</h1>
				<h5>Admin Panel</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row">
				<div class="col-6 align-self-center">
					<h4 class="green-color my-4">Events</h4>
				</div>
				<div class="col-6 pull-right align-self-center">
					<a href="" class="btn unique-color btn-rounded m-0 pull-right" data-toggle="modal" data-target="#modalAdd">ADD EVENTS</a>
				</div>
			</div>
			<?php

			require_once 'functions.php';

			$Events = Search_Query("SELECT * from events order by CreatedDate");

			foreach ($Events as $Key => $Event)
			{
				?>
				<div class="card">
					<div class="card-block p-sm-3">
						<div class="row">
							<?php 
							if ($Event['Image'] != 'na')
							{
								?>
								<div class="col-sm-5 col-md-4 col-lg-3">
									<div class="view overlay hm-white-slight rounded z-depth-1-half">
										<img src="../<?php echo $Event['Image'] ?>" class="img-fluid" style="max-height: 150px; margin: auto;" alt="<?php echo $Event['Title'] ?>">
										<a>
											<div class="mask waves-effect waves-light"></div>
										</a>
									</div>
								</div>
								<div class="col-sm-7 col-md-8 col-lg-9">
									<?php
								}
								else
									echo '<div class="col-12">';
								?>
								<div class="p-3 p-sm-0">
									<h5 class="font-bold dark-grey-text mb-2">
										<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light float-right" data-entity="events" data-id="<?php echo $Event['ID'] ?>"><i class="fa fa-remove"></i></a>
										<?php echo $Event['Title'] ?>
									</h5>
									<p class="grey-text mb-1"><?php echo $Event['Description'] ?></p>
									<p class="mb-1">Event Date: <?php echo $Event['EventDate'] ?></p>
									<p class="mb-1">Event Venue: <?php echo $Event['EventVenue'] ?></p>
									<p class="mb-0 small">Upload Date: <?php echo date('F j, Y, g:i a', strtotime($Event['CreatedDate'])) ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php echo ($Key < count($Events) - 1 ? '<hr>' : '') ?>
				<?php
			}
			?>

			<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog cascading-modal" role="document">
					<div class="modal-content">
						<div class="modal-header light-blue darken-3 white-text">
							<h4 class="title"><i class="fa fa-plus"></i> Add Event</h4>
							<button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<form method="post" id="form-event">
							<div class="modal-body mb-0">
								<div class="md-form">
									<input type="text" id="title" name="title" class="form-control" required>
									<label for="title">Title</label>
								</div>
								<div class="md-form">
									<input type="text" id="description" name="description" class="form-control" required>
									<label for="description">Description</label>
								</div>
								<div class="md-form">
									<input type="text" id="eventdate" name="eventdate" class="form-control" required>
									<label for="eventdate">Event Date &amp; Time</label>
								</div>
								<div class="md-form">
									<input type="text" id="eventvenue" name="eventvenue" class="form-control" required>
									<label for="eventvenue">Event Venue</label>
								</div>
								<label class="d-block">Image</label>
								<div class="md-form">
									<input type="file" id="image" name="image" class="formcontrol">
								</div>
							</div>
							<div class="modal-footer d-flex justify-content-center">
								<button type="submit" class="btn btn-primary" id="add-event">ADD EVENT</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script type="text/javascript" src="../js/bootbox.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			<?php 
			if (isset($_SESSION['toast-message']))
			{
				echo "toastr.success('".$_SESSION['toast-message']."', '', {positionClass: 'toast-bottom-left'});";
				unset($_SESSION['toast-message']);
			}
			?>

			$("#form-event").submit(function(e) {
				e.preventDefault();
				$("#add-event").prop('disabled', 'disabled')
				$("#add-event").html('<i class="fa fa-spinner fa-spin mr-2"></i> ADDING EVENT');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "addevent.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Event added successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-event")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#add-event").prop('disabled', '')
						$("#add-event").html('ADD EVENT');
					}
				});
			});

			$(".btn-delete").click(function() {
				var SelectedItem = $(this);
				bootbox.confirm({
					message: "Do you want to really wish to delete this event?",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'danger-color-dark'
						},
						cancel: {
							label: 'No',
							className: 'btn-green'
						}
					},
					callback: function (result) {
						if (result == true)
						{
							SelectedItem.prop('disabled', 'disabled')
							SelectedItem.html('<i class="fa fa-spinner fa-spin"></i>');
							$.ajax({
								type: "get",
								url: "deleteentity.php",
								data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity'),
								success: function (data) {
									if (data == 'true')
									{
										location.reload();
									}
									else
									{
										toastr.error(data, '', {positionClass: 'toast-bottom-left'});
									}
									SelectedItem.prop('disabled', '')
									SelectedItem.html('<i class="fa fa-remove"></i>');
								}
							});
						}
					}
				});
			});
		});
	</script>
</body>
</html>