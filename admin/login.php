<?php

if (!isset($_SESSION))
	session_start();

if (isset($_SESSION['login']))
{
	header('location: index.php');
	die();
}

$LoginError = false;
if (isset($_POST['username']) && isset($_POST['password']))
{
	require_once 'functions.php';
	$User = Search_Query("SELECT 1 from users where username = '".$_POST['username']."' and password = '".md5($_POST['password'])."'");
	if (count($User) > 0)
	{
		$_SESSION['login'] = 1;
		header('location: index.php');
		die();
	}

	$LoginError = true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="...images/favicon.ico" type="image/x-icon">
	<title>Login – Admin Panel – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/mdb.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
</head>
<body style="background: #ddd;">
	<nav class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1d4870;">
		<div class="container">
			<a class="navbar-brand" href="index.php">
				<img src="../images/logo.png" height="40" alt="">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav smooth-scroll ml-auto nav-flex-icons">
					<li class="nav-item section-nav-item text-white">
						Login
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<main>

		<div class="container flex-center mt-3" style="height: 70%">
			<div class="d-flex align-items-center" style="width: 500px">
				<form id="form-login" method="POST" action="login.php">
					<div class="card" style="width: 500px">
						<div class="card-body z-depth-2">
							<!--Header-->
							<div class="text-center">
								<h3>Login – Admin Panel</h3>
								<hr>
							</div>
							<div class="md-form">
								<i class="fa fa-user prefix grey-text"></i>
								<input type="text" id="form3" name="username" class="form-control">
								<label for="form3">Username</label>
							</div>
							<div class="md-form">
								<i class="fa fa-lock prefix grey-text"></i>
								<input type="password" id="form2" name="password" class="form-control">
								<label for="form2">Password</label>
							</div>
							<div class="text-center">
								<button class="btn btn-primary" type="submit">Login</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			<?php
			if ($LoginError)
			{
				echo "toastr.error('Incorrect username or password', '', {positionClass: 'toast-bottom-left'});";
			}
			?>
		});
	</script>
</body>

</html>