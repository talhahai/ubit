<?php

if (!isset($_SESSION))
	session_start();

if (!isset($_SESSION['login']))
{
	header('location: login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="...images/favicon.ico" type="image/x-icon">
	<title>Messages – Admin Panel – UBIT</title>
	<link rel="stylesheet" href="../css/font-awesome.min.css">
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/mdb.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Messages</h1>
				<h5>Admin Panel</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<h4 class="green-color my-4">Recent Messages</h4>
			<div class="card">
				<table class="table">
					<thead>
						<tr>
							<th class="font-weight-bold">Subject</th>
							<th class="font-weight-bold">Name</th>
							<th class="font-weight-bold">Email</th>
							<th class="text-center font-weight-bold" style="width: 100px">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php

						require_once 'functions.php';

						$Messages = Search_Query("SELECT * from messages order by sentdate desc");

						foreach ($Messages as $Message)
						{
							?>
							<tr>
								<td><?php echo $Message['Subject'] ?></td>
								<td><?php echo $Message['Name'] ?></td>
								<td><?php echo $Message['Email'] ?></td>
								<td class="align-middle text-center action-btn-2 p-0">
									<a class="m-0 btn-sm btn-floating danger-color-dark btn-delete waves-effect waves-light" data-entity="messages" data-id="<?php echo $Message['ID'] ?>"><i class="fa fa-remove"></i></a>
								</td>
							</tr>							
							<tr>
								<td colspan="3" style="border-top: none" class="pt-0"><b>Message: </b><?php echo $Message['Message'] ?></td>
							</tr>							
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			<?php 
			if (isset($_SESSION['toast-message']))
			{
				echo "toastr.success('".$_SESSION['toast-message']."', '', {positionClass: 'toast-bottom-left'});";
				unset($_SESSION['toast-message']);
			}
			?>

			$(".btn-delete").click(function() {
				var SelectedItem = $(this);
				$.ajax({
					type: "get",
					url: "deleteentity.php",
					data: "id="+SelectedItem.data('id')+"&entity="+SelectedItem.data('entity'),
					success: function (data) {
						if (data == 'true')
						{
							location.reload();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
					}
				});
			});
		});
	</script>
</body>
</html>