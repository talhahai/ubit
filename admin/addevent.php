<?php

require_once 'functions.php';

if (isset($_POST['title']) && isset($_POST['description']) && isset($_POST['eventdate']) && isset($_POST['eventvenue']))
{
	if (!empty($_POST['title']) && !empty($_POST['description']) && !empty($_POST['eventdate']) && !empty($_POST['eventvenue']))
	{
		$FileName = 'na';

		if(mysqli_query($Connection, "INSERT INTO events set
			title = '".mysqli_real_escape_string($Connection, $_POST['title'])."',
			description = '".mysqli_real_escape_string($Connection, $_POST['description'])."',
			eventdate = '".mysqli_real_escape_string($Connection, $_POST['eventdate'])."',
			eventvenue = '".mysqli_real_escape_string($Connection, $_POST['eventvenue'])."',
			Image = '".$FileName."',
			CreatedDate = '".date('Y-m-d H:i:s')."'"))
		{
			$EventID = mysqli_insert_id($Connection);

			if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"]))
			{
				$FileExtension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

				if ($FileExtension != 'png' && $FileExtension != 'jpg' && 
					$FileExtension != 'jpeg' && $FileExtension != 'zip')
				{
					echo "File format not suported";
					return;
				}

				$FileName = 'images/events/'.$EventID.'.'.$FileExtension;

				if (move_uploaded_file($_FILES['image']['tmp_name'], '../'.$FileName))
				{
					mysqli_query($Connection, "UPDATE events set Image = '".$FileName."' where id = '".$EventID."'");
				}
			}

			echo 'true';
			return;
		}
		else
		{	
			echo "Event not added, please try again later";
			return;
		}
	}
}

echo "Event not added, try to fill required fields";
return;

?>