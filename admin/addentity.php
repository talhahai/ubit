<?php

require_once 'functions.php';

if (isset($_POST['facultyid']) && isset($_POST['entity']) && isset($_POST['description']))
{
	if (!empty($_POST['facultyid']) && !empty($_POST['entity']) && !empty($_POST['description']))
	{
		if (in_array($_POST['entity'], ['educations', 'awards', 'interests']))
		{
			$MaxPriority = Search_Query("SELECT coalesce(max(priority),0) as 'MaxPriority' from ".$_POST['entity'])[0]['MaxPriority'] + 1;
			if(mysqli_query($Connection, "INSERT INTO ".$_POST['entity']." set
				facultyid = '".$_POST['facultyid']."',
				description = '".mysqli_real_escape_string($Connection, $_POST['description'])."',
				Priority = '".$MaxPriority."'"))
			{
				echo 'true';
				return;
			}
			else
			{	
				echo ucwords($_POST['entity'])." not added, please try again later";
				return;
			}
		}
	}
}

echo ucwords($_POST['entity'])." not added, try to fill required fields";
return;

?>