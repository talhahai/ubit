<?php

require_once 'functions.php';

if (!isset($_GET['id']) || !isset($_GET['entity']))
	return;

if(mysqli_query($Connection,  "DELETE FROM ".$_GET['entity']." where id = '".$_GET['id']."'"))
{
	if (!isset($_SESSION))
		session_start();

	unset($_SESSION['toast-message']);
	$_SESSION['toast-message'] = 'Deleted successfully';

	echo "true";
}
else
{			
	echo ucwords($_GET['entity'])." not deleted, please try again";
}

?>