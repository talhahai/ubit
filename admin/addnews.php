<?php

require_once 'functions.php';

if (isset($_POST['title']) && isset($_POST['description']))
{
	if (!empty($_POST['title']) && !empty($_POST['description']))
	{
		$FileName = 'na';

		if(mysqli_query($Connection, "INSERT INTO news set
			title = '".mysqli_real_escape_string($Connection, $_POST['title'])."',
			description = '".mysqli_real_escape_string($Connection, $_POST['description'])."',
			Image = '".$FileName."',
			CreatedDate = '".date('Y-m-d H:i:s')."'"))
		{
			$NewsID = mysqli_insert_id($Connection);

			if (isset($_FILES["image"]["name"]) && !empty($_FILES["image"]["name"]))
			{
				$FileExtension = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

				if ($FileExtension != 'png' && $FileExtension != 'jpg' && 
					$FileExtension != 'jpeg' && $FileExtension != 'zip')
				{
					echo "File format not suported";
					return;
				}

				$FileName = 'images/news/'.$NewsID.'.'.$FileExtension;

				if (move_uploaded_file($_FILES['image']['tmp_name'], '../'.$FileName))
				{
					mysqli_query($Connection, "UPDATE news set Image = '".$FileName."' where id = '".$NewsID."'");
				}
			}

			echo 'true';
			return;
		}
		else
		{	
			echo "News not added, please try again later";
			return;
		}
	}
}

echo "News not added, try to fill required fields";
return;

?>