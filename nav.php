<?php

$ScriptName = end(explode('/', $_SERVER['SCRIPT_NAME']));

if ($ScriptName == 'index.php')
	$NavAttr = 'class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar"';
else
	$NavAttr = 'class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1d4870;"';

?>
<nav <?php echo $NavAttr ?>>
	<div class="container">
		<a class="navbar-brand" href="index.php">
			<img src="images/logo.png" height="40" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav smooth-scroll ml-auto">
				<li class="nav-item section-nav-item">
					<a class="nav-link" href="<?php echo $ScriptName == 'index.php' ? '#home' : 'index.php' ?>">Home</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuAbout" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						About
					</a>
					<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuAbout" style="position: absolute;">
						<a class="dropdown-item waves-effect waves-light" href="aboutus.php">About UBIT</a>
						<a class="dropdown-item waves-effect waves-light" href="vision_mission.php">Vision &amp; Mission</a>
						<a class="dropdown-item waves-effect waves-light" href="chancellor_msg.php">Chancellor's Message</a>
						<a class="dropdown-item waves-effect waves-light" href="vicechancellor_msg.php">Vice Chancellor's Message</a>
						<a class="dropdown-item waves-effect waves-light" href="chairman_msg.php">Chairman's Message</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuPrograms" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Programs
					</a>
					<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuPrograms" style="position: absolute;">
						<a class="dropdown-item waves-effect waves-light" href="bscs.php">B.S.C.S.</a>
						<a class="dropdown-item waves-effect waves-light" href="bsse.php">B.S.S.E.</a>
						<a class="dropdown-item waves-effect waves-light" href="mcs.php">M.C.S.</a>
						<a class="dropdown-item waves-effect waves-light" href="ms_phd.php">M.S. / Ph.D.</a>
						<a class="dropdown-item waves-effect waves-light" href="gradepoint.php">Grade Point Table</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuAdminssions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Admissions
					</a>
					<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuAdminssions" style="position: absolute;">
						<a class="dropdown-item waves-effect waves-light" href="why_ubit.php">Why Choose UBIT</a>
						<a class="dropdown-item waves-effect waves-light" href="affiliated_colleges.php">Affiliated Colleges</a>
						<a class="dropdown-item waves-effect waves-light" href="fee_structure.php">Fees Structure</a>
						<a class="dropdown-item waves-effect waves-light" href="admission_policy.php">Admission Policy</a>
						<a class="dropdown-item waves-effect waves-light" href="eligibility_criteria.php">Eligibility Criteria</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="faculty.php">Faculty</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="researches.php">Researches</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbarDropdownMenuNewsEvents" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						News &amp; Events
					</a>
					<div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuNewsEvents" style="position: absolute;">
						<a class="dropdown-item waves-effect waves-light" href="news.php">News</a>
						<a class="dropdown-item waves-effect waves-light" href="events.php">Events</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>