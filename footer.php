<footer class="page-footer center-on-small-only mt-0 mdb-color darken-3" id="footer">
	<div class="container">
		<div class="row " data-wow-delay="0.2s">
			<div class="col-md-3 mb-md-3 mt-3">
				<div class="text-center mb-3">
					<img src="images/logo.png" class="img-fluid">
				</div>
				<p class="white-text text-justify mb-5">Get in touch with us. If you have any questions regarding programs, facilities or suggestions to make, feel free to to contact us.</p>
			</div>
			<div class="col-md-9 mb-3 mt-md-3 text-left">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-3">
						<h5>About Us</h5>
						<ul>
							<li><a href="aboutus.php">About UBIT</a></li>
							<li><a href="vision_mission.php">Vision &amp; Mission</a></li>
							<li><a href="chancellor_msg.php">Chancellor's Message</a></li>
							<li><a href="vicechancellor_msg.php">Vice Chancellor's Message</a></li>
							<li><a href="chairman_msg.php">Chairman's Message</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-6 col-md-3">
						<h5>Programs</h5>
						<ul>
							<li><a href="bscs.php">B.S.C.S.</a></li>
							<li><a href="bsse.php">B.S.S.E.</a></li>
							<li><a href="mcs.php">M.C.S.</a></li>
							<li><a href="ms_phd.php">M.S. / Ph.D.</a></li>
							<li><a href="gradepoint.php">Grade Point Table</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-6 col-md-3 mt-3 mt-md-0">
						<h5>Admissions</h5>
						<ul>
							<li><a href="why_ubit.php">Why Choose UBIT</a></li>
							<li><a href="affiliated_colleges.php">Affiliated Colleges</a></li>
							<li><a href="fee_structure.php">Fees Structure</a></li>
							<li><a href="admission_policy.php">Admission Policy</a></li>
							<li><a href="eligibility_criteria.php">Eligibility Criteria</a></li>
						</ul>
					</div>
					<div class="col-12 col-sm-6 col-md-3 mt-3 mt-md-0">
						<h5 class="mb-3"><a href="faculty.php">Faculty</a></h5>
						<h5 class="mb-3"><a href="researches.php">Researches</a></h5>
						<h5>News &amp; Events</h5>
						<ul>
							<li><a href="news.php">News</a></li>
							<li><a href="events.php">Events</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="footer-copyright">
		<div class="container-fluid">
			© 2017 Copyright: <a href="http://www.MDBootstrap.com"> WebDroid Solutions </a>
		</div>
	</div>
</footer>