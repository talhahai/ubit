<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Fees Structure – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Fees Structure</h1>
				<h5>Admissions</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="why_ubit.php" class="list-group-item grey lighten-4">Why Choose UBIT</a>
						<a href="affiliated_colleges.php" class="list-group-item grey lighten-4">Affiliated Colleges</a>
						<a href="fee_structure.php" class="list-group-item active">Fees Structure</a>
						<a href="admission_policy.php" class="list-group-item grey lighten-4">Admission Policy</a>
						<a href="eligibility_criteria.php" class="list-group-item grey lighten-4">Eligibility Criteria</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color">Fees Structure</h4>
					<table class="table table-bordered course-table">
						<tbody>
							<tr>
								<th></th>
								<th>B.S.C.S.</th>
								<th>B.S.S.E.</th>
								<th>M.C.S</th>
								<th>M.S. / Ph.D.</th>
							</tr>
							<tr>
								<th>Admission Fee</th>
								<td>3,000</td>
								<td>3,000</td>
								<td>3,000</td>
								<td>3,000</td>
							</tr>
							<tr>
								<th>Security Fee</th>
								<td>5,000</td>
								<td>5,000</td>
								<td>5,000</td>
								<td>5,000</td>
							</tr>
							<tr>
								<th>Admission Processing Fee</th>
								<td>7,000</td>
								<td>7,000</td>
								<td>7,000</td>
								<td>7,000</td>
							</tr>
							<tr>
								<th>Enrollment FeesPer Semester</th>
								<td>15,000</td>
								<td>15,000</td>
								<td>15,000</td>
								<td>15,000</td>
							</tr>
							<tr>
								<th>Tuition Feesper credit hour</th>
								<td>20,000</td>
								<td>20,000</td>
								<td>20,000</td>
								<td>20,000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js">
	</script>
	<script type="text/javascript" src="js/popper.min.js">
	</script>
	<script type="text/javascript" src="js/bootstrap.min.js">
	</script>
	<script type="text/javascript" src="js/mdb.min.js">
	</script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>