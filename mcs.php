<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>M.C.S. – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Masters in Computer Science</h1>
				<h5>Programs</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="bscs.php" class="list-group-item grey lighten-4">B.S.C.S.</a>
						<a href="bsse.php" class="list-group-item grey lighten-4">B.S.S.E.</a>
						<a href="mcs.php" class="list-group-item active">M.C.S.</a>
						<ul class="nav flex-column smooth-scroll-custom" id="nav-scrollspy" role="navigation">
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#about" role="tab">About M.C.S.</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#compulsory" role="tab">Compulsory Courses</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#optional" role="tab">Optional Courses</a>
							</li>
						</ul>
						<a href="ms_phd.php" class="list-group-item grey lighten-4">M.S. / Ph.D.</a>
						<a href="gradepoint.php" class="list-group-item grey lighten-4">Grade Point Table</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color" id="about">Masters in Computer Science</h4>
					<ul>
						<li>Duration of the program, Two years.</li>
						<li>Number of semesters, Four (Two semester every year)</li>
					</ul>

					<h4 class="green-color my-4">Masters in Computer Science Courses</h4>

					<h5 class="green-color text-center" id="compulsory">Compulsory Courses</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS-501</td>
								<td>Fundamentals of Digital Computing</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-502</td>
								<td>Automata Theory</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>CS-503</td>
								<td>Statistical Methods</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-504</td>
								<td>Statistical Methods</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-505</td>
								<td>Numerical Computing - I</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-506</td>
								<td>Numerical Computing - II</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-507</td>
								<td>Linear Programming</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-508</td>
								<td>Linear Programming Advanced</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-509</td>
								<td>Programming Languages</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-510</td>
								<td>Programming Languages Advanced</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-511</td>
								<td>Models &amp; Inference</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-512</td>
								<td>Advanced Assembly Language</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-513</td>
								<td>Computer Architecture</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-601</td>
								<td>Database Management Systems</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-602</td>
								<td>Structured Programming</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-603</td>
								<td>Operations Research</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-604</td>
								<td>Operations Research Advanced</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-605</td>
								<td>Digital &amp; Analog Computers and Servo Mechanism</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-606</td>
								<td>Decision Theory</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-607</td>
								<td>Numerical Computing Advanced - I</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-608</td>
								<td>Numerical Computing Advanced - II</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-609</td>
								<td>Introductory Computer Simulation</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-610</td>
								<td>Applications of Computer Simulation</td>
								<td>2+1</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="optional">Optional Courses</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong>Optional</strong></td>
								<td></td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-611</td>
								<td>System Analysis &amp; Design - I</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-612</td>
								<td>System Analysis &amp; Design - II</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-613</td>
								<td>Operating Systems</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-614</td>
								<td>Advanced Cobol Programming</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-615</td>
								<td>Software Engineering</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-616</td>
								<td>Artificial Intelligence</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-617</td>
								<td>Expert Systems</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-618</td>
								<td>Computer Graphics</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-619</td>
								<td>Microprocessor and Applications</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-620</td>
								<td>Compiler Construction</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-621</td>
								<td>Graph Theory</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-622</td>
								<td>Data Communications &amp; Networking</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS-691</td>
								<td>Thesis</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS-692</td>
								<td>Thesis</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS-693</td>
								<td>Project</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>