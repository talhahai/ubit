<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Why Choose UBIT – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Why Choose UBIT</h1>
				<h5>Admissions</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="why_ubit.php" class="list-group-item active">Why Choose UBIT</a>
						<ul class="nav flex-column smooth-scroll-custom" id="nav-scrollspy" role="navigation">
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#objective" role="tab">Objective of UBIT</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#structure" role="tab">Structure of UBIT</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#programsOffered" role="tab">Programs Offered</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#curriculumPolicy" role="tab">Curriculum Policy</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#careerOpportunities" role="tab">Career Opportunities</a>
							</li>
						</ul>
						<a href="affiliated_colleges.php" class="list-group-item grey lighten-4">Affiliated Colleges</a>
						<a href="fee_structure.php" class="list-group-item grey lighten-4">Fees Structure</a>
						<a href="admission_policy.php" class="list-group-item grey lighten-4">Admission Policy</a>
						<a href="eligibility_criteria.php" class="list-group-item grey lighten-4">Eligibility Criteria</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color" id="objective">Objective of UBIT</h4>
					<p>The main objective of the UBIT is to impart quality education and conduct research in computer science and information technology. Through structured degree programs, intensive training courses for professionals, academicians, and researchers by organizing workshops and training courses. The main elements of the program focus on human research development in the field of IT. Specially designed research projects in computers, hardware & software engineering, IT, telecommunications software technologies, high tech data analysis techniques and modern trends in CS/IT. The UBIT complex is comprised of three blocks. The institute has state-of-the-art computer facilities and leased line circuits. There are several lecture rooms, discussion rooms, computer laboratories, and faculty offices. Rooms for holding teleconferencing, discussions, and training sessions are also provided for sessions on specialized topics. Library, administrative offices, filing room, prayer rooms, ladies common room also form a part of the UBIT establishment. The UBIT complex was completed in 2006. Some facts about the complex are as follows:</p>
					
					<h4 class="green-color mt-4" id="structure">Structure of UBIT</h4>
					<table class="table table-bordered course-table">
						<tbody>
							<tr>
								<td>Plot area</td>
								<td>9.75 Acre</td>
							</tr>
							<tr>
								<td>Electrical / Mechanical room</td>
								<td>05</td>
							</tr>
							<tr>
								<td>Builing covered area</td>
								<td>21,000 sq. ft.</td>
							</tr>
							<tr>
								<td>Server room</td>
								<td>1</td>
							</tr>
							<tr>
								<td>Number of floors</td>
								<td>3</td>
							</tr>
							<tr>
								<td>Library</td>
								<td>1</td>
							</tr>
							<tr>
								<td>Class rooms</td>
								<td>12 for 1500 students</td>
							</tr>
							<tr>
								<td>Senimar hall</td>
								<td>1</td>
							</tr>
							<tr>
								<td>Computer labs</td>
								<td>7 for 840 students</td>
							</tr>
							<tr>
								<td>Prayer room</td>
								<td>1</td>
							</tr>
							<tr>
								<td>Faculty room</td>
								<td>18 for 50 teachers</td>
							</tr>
							<tr>
								<td>Miscellaneous rooms</td>
								<td>11</td>
							</tr>
							<tr>
								<td>Accounts / Admin room</td>
								<td>2</td>
							</tr>
							<tr>
								<td>Gas fired generator</td>
								<td>1 - 350 KVA</td>
							</tr>
						</tbody>
					</table>
					
					<h4 class="green-color mt-4" id="programsOffered">Programs Offered</h4>
					<ul>
						<li>B.S. Computer Science</li>
						<li>B.S. Software Engineering</li>
						<li>Masters in Computer Science</li>
						<li>M.S. / Ph.D.</li>
					</ul>
					
					<h4 class="green-color mt-4" id="curriculumPolicy">Curriculum Policy</h4>
					<p>The contents of courses offered are revised after every two years and books are recommended to cope with the rapid developments which are taking place in Computer Science and Information Technology. All the curricula have been designed in the light of recommendations of Task Force (on the Curriculum for Computer Science Programs for general Universities of Sindh). Thus all the curricula of Department of Computer Science are developed for CS/IT on the following policy:</p>
					<p>"Curriculum should be made flexible so as to be responsive to the changing g structure of the market. The curricula shall encourage thinking, creativity and project construction ability. The curricula of CS/IT shall be made comparable with international standards and matching the needs of the next century by developing libraries, laboratories, and above all teaching staff".</p>

					<h4 class="green-color mt-4" id="careerOpportunities">Career Opportunities</h4>
					<p>Excellent career opportunities exist for the graduates of Department of Computer Science. More than 80% of graduates are immediately hired by top ranking software houses and other institutions.</p>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>