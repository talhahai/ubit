-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 12, 2018 at 01:06 PM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ubit`
--
CREATE DATABASE IF NOT EXISTS `ubit` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ubit`;

--
-- Truncate table before insert `awards`
--

TRUNCATE TABLE `awards`;
--
-- Dumping data for table `awards`
--

INSERT INTO `awards` (`ID`, `FacultyID`, `Description`, `Priority`) VALUES
(1, 1, 'Best IT Educationist â€“ NCR 2002-3', 1),
(2, 1, 'ISSOS shield best Statistician 1998', 2),
(3, 1, 'Best IT Academician Award 2002-2003', 3),
(4, 1, 'Research Award 2003-4', 4),
(5, 1, 'Many awards and shields as best speaker on IT/CS', 5),
(6, 6, 'Honorary Post Doctoral Research Fellow, COSIPRA lab, University of Stirling', 6),
(7, 13, 'Most Active Participant in IEEEXplore quiz', 7),
(8, 14, '3rd position in MS Computer Science, from IBA', 8),
(9, 14, '1st position in BS Computer Science, from DCS University of Karachi', 9);

--
-- Truncate table before insert `educations`
--

TRUNCATE TABLE `educations`;
--
-- Dumping data for table `educations`
--

INSERT INTO `educations` (`ID`, `FacultyID`, `Description`, `Priority`) VALUES
(1, 1, '2013 â€“ Certificate Training, Supply Chain Management modeling development, from IoBM', 1),
(2, 1, '1994 â€“ Certification in Quality Management and Library Services, from Edinboro University of Pennsylvania', 2),
(3, 1, '1987 â€“ Ph.D. Mathematics Algorithemic Analysis Design, Simulation and Modeling for Multivariate Time Series Anal, from University of Strathclyde', 3),
(4, 1, '1983 â€“ Certificate In Demographic Studies, Demography and Population Studies, from Asian Institute of Statistics(UN)', 4),
(5, 1, '1983 â€“ MPhil, Risk Theory and Insurance Modeling and Simulation A stochastic Approach, from University of Karachi', 5),
(6, 1, '1972 â€“ Master of Science (MSc), Statistics, from University of Karachi', 6),
(7, 1, '1970 â€“ BSc, from DJ Science College', 7),
(8, 1, '1968 â€“ HSC, from Jamie Millia College', 8),
(9, 2, 'Ph.D., from University of Karachi', 9),
(10, 2, 'Bachelors of Science, from SSUET', 10),
(11, 2, 'Master of Science, from SSUET', 11),
(13, 3, '2010 â€“ Ph.D. in Computer Science, from University of Karachi', 14),
(14, 3, '1996 â€“ Masters in Computer Science, from Cornell University of Karachi', 15),
(15, 3, '2014 â€“ Post-Doctoral Research, from International Islamic University Malaysia', 13),
(16, 4, 'Ph.D. (Candid) from University of Karachi', 16),
(17, 4, 'Masters in Computer Science, from University of Karachi', 17),
(18, 4, 'Bachelors of Science', 18),
(19, 5, 'Master in Computer Science, from University of Karachi', 19),
(20, 5, 'Bachelors of Science', 20),
(21, 6, '2007 â€“ Ph.D. (Computer Science), from University of Karachi', 21),
(22, 6, '2003 â€“ MA (Economics), from University of Karachi', 22),
(23, 6, '2001 â€“ MSc (Statistics), from University of Karachi', 23),
(24, 6, '1998 â€“ BSc (Computer Sciences, Statistics, Mathematics), from University of Karachi', 24),
(25, 7, 'In Progress â€“ Ph.D. Computer Science, from University of Karachi', 25),
(26, 7, '2003 -MS Computer Science, from Lahore University of Management Sciences', 26),
(27, 7, '2000 â€“ BS Computer Science, from University of Karachi', 27),
(28, 8, 'In Progress â€“ Ph.D., from University of Karachi', 28),
(29, 8, '2007 â€“ Post Graduation Diploma in Statistics and Data Engineering, from University of Karachi', 29),
(30, 8, '2001 â€“ Master in Computer Science, from University of Karachi', 30),
(31, 8, '1997 â€“ B.S. (Electronics Engineering), from SSUET', 31),
(32, 9, 'In Progress â€“ Ph.D. Computational Finance (Computer Science / Finance), from University of Karachi', 32),
(33, 9, '2008 â€“ M. S., Computer Science, from National University of Computer and Emerging Sciences', 33),
(34, 9, '2005 â€“ M.B.A Finance, from University of Karachi', 34),
(35, 9, '2000 â€“ BS Computer Science, from University of Karachi', 35),
(36, 10, '2002 â€“ BS(CS), Computer Science,', 36),
(37, 10, '2005 â€“ MA, Mass Communication / Mgmt. & Org. Communication, from University of Karachi', 37),
(38, 10, '2007 â€“ MS, Management Sciences, from SZABIST', 38),
(39, 10, '2015 â€“ Ph.D, Computer Science / MIS, from University of Karachi', 39),
(40, 11, 'In Progress â€“ Ph.D (Candid), from University of Karachi', 40),
(41, 11, '2012 â€“ MS (Computer Networks & Communications), from Hamdard University', 41),
(42, 11, '2005 â€“  MBA (Finance), from University of Karachi', 42),
(43, 11, '2001 â€“ BS (Computer Engineering),  from Sir Syed University of Engineering & Technology', 43),
(45, 12, 'She received her B.E (Electrical) degree from NED University of Engineering and Technology in 1999  and then  continue her studies at University of Karachi for Masters of Computer Science (MCS) in 2001. She stands First Class First amongst the Evening batch of MCS 2001-2003.  Dr. Humera Tariq joined MS leading to PhD program in  2009 and completed MS course work with  CGPA 4.0. She started her PhD work in the field of image processing in 2011 under the supervision of Meritorious Professor Dr. S.M. Aqil Burney.', 44),
(46, 13, '2016 â€“ Ph.D. Computer Science, from University of Karachi', 45),
(47, 13, '2011 â€“ MS Computer Science, from University of Karachi', 46),
(48, 13, '2006 â€“ BS Computer Science, from University of Karachi', 47),
(49, 14, 'Enrolled in Ph.D, from University of Karachi', 48),
(50, 14, '2013 â€“ Master of Science (MS), Computer Science, from IBA', 49),
(51, 14, '2009 â€“ Master in Business Administration, from KUBS', 50),
(52, 14, '2006 â€“ Bachelor of Science (BS), Computer Science, from University of Karachi', 51),
(53, 15, 'MS/PhD (Candid), BSCS (KU).', 52),
(54, 15, '2007 â€“ Bachelor of Science (BS), Computer Science, from University of Karachi with first class first position.', 53),
(55, 15, '2002 â€“ HSSC, Computer Science, Bahria College, NORE-I, Karachi.', 54),
(56, 16, 'Mechanical Engineering, from NED University of Engineering and Technology', 55),
(57, 16, 'Bachelor of Science, from Sir Syed University of Engineering and Technology', 56),
(58, 17, 'Master of Science (MS),(in Progress), from University of Karachi.', 57),
(59, 17, 'Master in Computer Science (MCS), from University of Karachi.', 58),
(60, 18, 'Master of Computer Science, from University of Karachi', 59),
(61, 18, 'Enrolled â€“ Master of Science in Computer Science, from University of Karachi', 60),
(62, 21, 'Masters in Computer Science, from University of Karachi', 61);

--
-- Truncate table before insert `events`
--

TRUNCATE TABLE `events`;
--
-- Dumping data for table `events`
--

INSERT INTO `events` (`ID`, `Title`, `Description`, `Image`, `EventDate`, `EventVenue`, `CreatedDate`) VALUES
(1, 'Event 1', 'IT Event', 'na', '20-Feb-2018', 'UBIT', '2018-02-11 21:28:42'),
(2, 'Event 2', 'General Conference', 'na', '22-Feb-2018 @ 10 pm', 'Arts Auditorium', '2018-02-11 21:29:18');

--
-- Truncate table before insert `faculty`
--

TRUNCATE TABLE `faculty`;
--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`ID`, `Name`, `Designation`, `Image`, `Biography`, `Email`, `Degrees`, `Quote`, `Priority`) VALUES
(1, 'Prof Dr S.M. Aqil Burney', 'Founder Director', 'images/faculty/1.png', 'Prof Dr S.M. Aqil Burney has served well over 42 years at University of Karachi. Now Dr. Aqil Burney is Professor at College of Computer Science and Information Systems (CCSIS) at Institute of Business Management (IoBM)Karachi, one of the leading Business Schools of the country.\r\n\r\nDr. Aqil Burney was a Meritorious Professor (R.E.) and approved supervisor in Computer Science and Statistics by the Higher Education Commission, Govt. of Pakistan. He was also the founder Director (UBIT) & Chairman of the Department of Computer Science, University of Karachi. He is also member of various higher academic boards of different universities of Pakistan.\r\n\r\nHis research interest includes artificial intelligence, soft computing, neural networks, fuzzy logic, data mining, statistics, simulation and stochastic modeling of mobile communication system and networks and network security, ,Now heading the detp. of actuarial science and risk management at CSCIS â€“ IoBM.Teaching mostly MS(CS) PhD(CS))courses such as Data Warehousing,data mining & ML and information retrieval systems,fuzzy systems ,advanced theory of statistics, Markov chains and Financial Time Series.\r\n\r\nHe is author/co-author of five books, various technical reports and has published more than 145 research papers in national and international research journals and has attended around 60 national and international conferences/seminars/symposiums with more than 470 citations. He has supervised hundreds of projects in operation research, statistics, simulation and modeling, software engineering and intelligent systems,Computer science , stochastic processes and biological/ physical sciences. Twelve research scholars completed Ph.D and five research students completed M Phil / M.S under his supervision in mathematical and computing sciences. At present more than five M.S./Ph.D. scholars are working under his supervision in the field of CS/IT and statistics in CS/IT.', 'burney@uok.edu.pk', 'Ph.D(UK), M.Phil.(KU), M.Sc.(KU)', '', 1),
(2, 'Dr. M. Sadiq Ali Khan', 'Chairman', 'images/faculty/2.png', 'Dr. M. Sadiq Ali Khan is Chairman of Department of Computer Science â€“ UBIT. He completed his Ph.D in 2011 under the supervision of Prof. Dr. S.M. Aqil Burney.\r\n\r\nDr. M. Sadiq Ali Khan had earned his Bachelors and Masters degree from Sir Syed University of Engineering and Technology. His main areas of interests are Network Security, Computer Networking, Software Engineering, Educational Administration, Security Information Technology', 'msakhan@uok.edu.pk', 'PhD (KU), MS (SSUET), BS (SSUET)', '', 2),
(3, 'Dr. Nadeem Mahmood', 'Associate Professor', 'images/faculty/3.png', 'Dr. Nadeem Mahmood is affiliated with the Department of Computer Science, University of Karachi as an Associate Professor. He has been involved in teaching and research at graduate and post graduate level for the last sixteen years.\r\n\r\nHe completed one year Post-Doctoral research at Faculty of Information and Communication Technology, International Islamic University Malaysia. He has obtained his MCS and Ph.D. in Computer Science from University of Karachi in 1996 and 2010 respectively.\r\n\r\nHe served as a member of HEC National curriculum committee for software engineering in 2009 and is also working as member national standard committee for IT/ICT, PSQCA, Ministry of Science and Technology.\r\n\r\nHe has published more than 30 research papers in international journals and conference proceedings (IEEE, ACM and WSEAS). He is working as technical and program committee member for different international journals and conferences. He is also a co-author of an international book published from Germany.', 'nmahmood@uok.edu.pk', 'Post. Doc. (IIU Malaysia), PhD (KU), MCS (KU)', '', 3),
(4, 'Mr. Badar Sami', 'Assistant Professor', 'images/faculty/4.png', 'Mr. Badar Sami is Assistant Professor at Department of Computer Science â€“ UBIT. According to Mr. Badar Sami, the goal of the education is to have a peaceful, vibrant society, so that we can interact with other people in a peaceful manner.\r\n\r\nHe has done his Bachelors in Science and then he did Masters in Computer Science from University of Karachi. Software Development and programming are his main fields.\r\n\r\nCurrently he is doing Ph.D in Statistical techniques in Text Mining of information retrieval systems. His Ph.D. is in final stages and is supervised by Prof. Dr. S.M. Aqil Burney', 'badarsami@uok.edu.pk', 'PhD (Candid), MCS(KU), B.Sc.', '', 4),
(5, 'Mr. Syed Jamal Hussain', 'Assistant Professor', 'images/faculty/5.png', 'Mr. Syed Jamal Hussain is Assistant Professor at Department of Computer and his hard-work for this department can never be neglected. He is one of  the senior teachers who is eager his knowledge and experiences with the students.\r\n\r\nMr. Syed Jamal Hussainâ€™s teaching style is unique. He blends well with the level of students and makes it easier for the students to understand difficult topics.', 'jamal@uok.edu.pk', 'MCS(KU), B.Sc.', '', 5),
(6, 'Dr. Tahseen Ahmed Jilani', 'Assistant Professor', 'images/faculty/6.png', 'Dr. Tahseen Ahmed Jilani has done B.Sc. in Computer Science. After that, he did double graduation, first one in Statistics and other in Economics. Before the double graduation, he learned Oracle( SQL, PL, D.B.A. track). Additionally, he had the experience to learn many programming languages as well.\r\n\r\nThe academic track in CS/IT and Statistics, allowed him to move in the fields of data science. (Data visualization, Machine learning, Decision theory, Regression models, Intelligent systems etc)', 'tahseenjilani@uok.edu.pk', 'PhD (KU), MSc (KU)', 'I am highly motivated by my teachers to work in the field of data science. This gave me the passion to work on R&D and develop new models and empirical decisions. Some of them are highly cited and used by other researchers and practitioners.', 6),
(7, 'Dr. Muhammad Saeed', 'Assistant Professor', 'images/faculty/7.png', 'Dr. Muhammad Saeed is Assistant Professor at Department of Computer Science â€“ UBIT. He is one of the most hardworking and dedicated teacher of DCS â€“ UBIT and his work for this department can not be neglected.\r\n\r\nAfter completion of BS in Computer Science from University of Karachi he moved to Lahore to earn MS Computer Science degree from Lahore University of Management and Scoence. He has been awarded with Ph.D. degree in 2015 under the Supervision of Prof. Dr. Nasir Touheed.\r\n\r\nDr. Muhammad Saeed has expertise in Advanced Computer Science, Computer Science Project, Data Structure and  Algorithms, Design & Analysis of Algorithm, Object Oriented Programming Techniques, Data Mining, Networking and many more.', 'saeed@uok.edu.pk', 'PhD (KU), MS (LUMS), BS (KU)', '', 9),
(8, 'Mr. Hussain Saleem', 'Assistant Professor', 'images/faculty/8.png', '', 'hussainsaleem@uok.edu.pk', 'Ph.D (Candid), MCS( KU), BS (Electronics Engg, SSUET) MPEC', '', 7),
(9, 'Mr. S. M. Khalid Jamal', 'Assistant Professor', 'images/faculty/9.png', 'S. M. Khalid Jamal is an Academic Scholar, Researcher, Faculty Member and Independent Information Technology and Services Consultant and is associated as Assistant Professor with Department of Computer Science, University of Karachi since 2001.\r\n\r\nS. M. Khalid Jamal is also engaged in Information technology and Services Consultancy. Consulting is a knowledge-intensive service. Major technology waves impact organizations in a profound way; basic objective of Technology consulting is to ride these waves by leveraging emerging technologies.\r\n\r\nAdvisory services are provided to help enhance performance of the Information Technology function and also to identify and mitigate risks introduced by IT in an organization. He is currently author of various research publications at National & International level.', 's.m.khalid@uok.edu.pk', 'Ph.D.(Candid) M.Phil(C/Fi), M.B.A (KU), BS (KU)', '', 8),
(10, 'Mr. Syed Asim Ali', 'Assistant Professor / Web Advisor', 'images/faculty/10.png', 'Integrity driven professional offering over fifteen years of experience at national and international level in Management of I.T, Networking, Business Process Re-engineering, Electronic public relations, Change Management, training and development.\r\n\r\nSyed Asim Ali has been teaching graduate level courses of Management Information System, Software Engineering,Operating System, System Analysis and Design Multimedia and Business Communication. He has been teaching at the Department of Public Administration and Department of Political Science. Mr. Syed Asim Ali developed courses of MIS, Software Engineering, Multimedia and Business Communication\r\n\r\nHe is the member of the 5 men team that designed, developed and deployed over 3000 nodes Fiber Optic based Network with Wireless Computing Support at University of Karachi. Over 22 km Fiber Optic Cable was used to complete what is the largest Fiber Optic Network in Asia.', 'asim@uok.edu.pk', 'PhD (KU), M.A. (KU), BS (KU)', '', 10),
(11, 'Mr. Farhan Ahmed Siddiqui', 'Assistant Professor / Students'' Advisor ', 'images/faculty/11.png', 'Mr. Farhan Ahmed Siddiqui is the Assistant Professor & Departmental Studentsâ€™ Advisor at the Department of Computer Science â€“ UBIT. He is one of the most selfless and dedicated teachers who is always ready to help students when needed.\r\n\r\nMr. Farhan Ahmed Siddiqui is doing his Ph.D from University of Karachi under the supervision of Prof. Dr. S.M. Aqil Burney . He has done his MS in Computer Networks and Communication & MBA in Finance from Hamdard and Karachi University respectively.\r\n\r\nHis Area of expertise are Advanced Computer Networks, Computer Network Security, Computer Communication & Network and Data Communication and Networking. He is one of the best teacher and always ready to help students  in every problem.', 'farhan@uok.edu.pk', 'PhD (Candid), MS (Comp. Networks), MBA(KU), BS (SSUET), MCP, MPEC, MIEEE', '', 11),
(12, 'Dr. Humera Tariq', 'Assistant Professor / Departmental Student''s Advisor', 'images/faculty/12.png', 'Dr. Humera Tariq has been associated with Department of Computer Science since year 2004.   Her research interest in recent times is Segmentation of Brain MR Axial Slices as a part of her PhD work. Other research interests and activities at Department of Computer science include final semester project supervision including Fog Removal from Images and Pb- lite Algorithm for Edge Detection.\r\n\r\nDuring years 2004 till 2014, she preferred to work on projects based on Image Processing, Computer Graphics, Modeling and Simulation and/or based on Current Technologies having some core computer science concepts. Presentations, Discussions, Meetings/Coordination are mandatory exercises for thesis, FYP or Semester Projects.\r\n\r\nDr. Humera Tariq joined the Department of Computer Science, University of Karachi as Assistant Professor in 2011. Before this position, she served as Lecturer at the same department from 2006 till 2011 . She also worked as cooperative teacher at the very same department from 2004 to 2006.  Prior to serving University of Karachi, she worked at Mohammad Ali Jinnah University (MAJU) as Assistant Professor from 2003 to 2004. She got her first university teaching experience from PAF KIET University during year 2001 as a visiting faculty in its computer science department.', 'humera@uok.edu.pk', 'BE, MCS, MS/PhD(KU)', '', 12),
(13, 'Dr. Zain Abbas', 'Assistance Professor', 'images/faculty/13.png', 'Dr. Zain Abbas is Assistance Professor in Department of Computer Science. He also served as a visiting faculty for 7 years and taught variety of CS courses such ICS, Programming, OOP, Web technologies, Discrete Math, OS, Software Testing, and Numerical Computing.\r\n\r\nHe also served as a Web Developer at University of Karachi for more than 7 years. Dr. Zain Abbas has fundamental knowledge and working experience of big data components such as Hadoop, Spark, Hive, Sqoop, Flume, HBase, MapReduce, Hortonworks Data Platform and AWS. He also has Deep understanding of statistical and predictive modeling, machine learning, clustering, classification, recommendation and optimization algorithms.', 'zain@uok.edu.pk', 'PhD (KU), BSCS (KU)', '', 13),
(14, 'Ms. Maryam Feroze', 'Lecturer', 'images/faculty/14.png', 'She started teaching at Department of Computer Science â€“ UBIT in January 2008 as visiting faculty and now she is lecturer here.', '', 'MSCS (IBA), MBA (KUBS), BSCS(KU)', 'I always enjoyed teaching even before joining at professional level. I used to teach my class mates and juniors during my studies from school life to university life and also at home.', 14),
(15, 'Ms. Humera Azam', 'Lecturer', 'images/faculty/na-female.png', 'Miss Humera Azam started teaching at Department of Computer Science â€“ UBIT in 2008.She served as a full time cooperative teacher in DCS â€“ UBIT, University of Karachi from June 2008 to 2013. After she joined as a lecturer in Department of Computer Science. She completed her Bachelor of Science (BS), Computer Science, from University of Karachi with first class first position.', 'humera.azam@uok.edu.pk', '', '', 15),
(16, 'Mr. Taha Bin Niaz', 'Lecturer', 'images/faculty/16.png', '', 'tniaz@uok.edu.pk', 'M.Engg. (NEDUET), BS (SSUET)', '', 17),
(17, 'Ms. Shaista Rais', 'Lecturer', 'images/faculty/na-female.png', '', 'shaista.rais@uok.edu.pk', '', '', 16),
(18, 'Ms. Madiha Khurram', 'Lecturer', 'images/faculty/na-female.png', '', 'madiha.khurram@uok.edu.pk', '', '', 18),
(19, 'Ms. Erum Shahid', 'Lecturer', 'images/faculty/na-female.png', '', 'eshahid@uok.edu.pk', '', '', 19),
(20, 'Mr. M. Naveed Anwer', 'Lecturer', 'images/faculty/na-male.png', NULL, NULL, NULL, NULL, 20),
(21, 'Mr. Jameel Ahmed', 'Lecturer', 'images/faculty/na-male.png', NULL, NULL, NULL, NULL, 21),
(22, 'Mr. Muhammad Sajid', 'Lecturer', 'images/faculty/na-male.png', '', 'msajid@uok.edu.pk', '', '', 22),
(23, 'Ms. Aemon Abdul Razzque', 'Full Time Cooperative', 'images/faculty/na-female.png', NULL, NULL, NULL, NULL, 23),
(24, 'Ms. Amna Ifhtikhar', 'Full Time Cooperative', 'images/faculty/na-female.png', NULL, NULL, NULL, NULL, 24),
(25, 'Mr. Saad Akber', 'Full Time Cooperative', 'images/faculty/na-male.png', NULL, NULL, NULL, NULL, 25),
(26, 'Mr. S. Meesam Ali Zaidi', 'Full Time Cooperative', 'images/faculty/na-male.png', NULL, NULL, NULL, NULL, 26),
(27, 'Mr. Usman Amjad', 'Full Time Cooperative', 'images/faculty/na-male.png', NULL, NULL, NULL, NULL, 27);

--
-- Truncate table before insert `interests`
--

TRUNCATE TABLE `interests`;
--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`ID`, `FacultyID`, `Description`, `Priority`) VALUES
(1, 1, 'Probability', 1),
(2, 1, 'Inference', 2),
(3, 1, 'Operations Research', 3),
(4, 1, 'Econometrics', 4),
(5, 1, 'Numerical analysis', 5),
(6, 1, 'Regression and Design', 6),
(7, 1, 'Time series', 7),
(8, 1, 'Demography', 8),
(9, 1, 'Simulation and Modeling', 9),
(10, 1, 'Computing', 10),
(11, 1, 'Artificial Intelligence', 11),
(12, 1, 'Data Mining', 12),
(13, 2, 'Network Security', 13),
(14, 2, 'Computer Networking', 14),
(15, 2, 'Software Engineering', 15),
(16, 2, 'Educational Administration', 16),
(17, 2, 'Security Information Technology', 17),
(18, 3, 'Temporal and Spatial Database Systems', 18),
(19, 3, 'Artificial Intelligence', 19),
(20, 3, 'Health Information Systems', 20),
(21, 3, 'Software Engineering', 21),
(22, 4, 'Software Development', 22),
(23, 4, 'Information Management & Operating System', 23),
(24, 5, 'Programming', 24),
(25, 5, 'Data Mining', 25),
(26, 5, 'Data Modeling', 26),
(27, 5, 'Artificial Intelligence', 27),
(28, 5, 'Machine Learning', 28),
(29, 6, 'Data Mining solutions for Big data', 29),
(30, 6, 'Intelligent Systems: Automated /adaptive solution for problems that evolve with time/space', 30),
(31, 6, 'Financial Modeling using AI and Machine Learning Techniques', 31),
(32, 6, 'E-health solutions', 32),
(33, 6, 'Probabilistic Models', 33),
(34, 6, 'Deep Learning Models', 34),
(35, 6, 'Statistical Techniques for usual and complex problems', 35),
(36, 6, 'System Simulation and modeling', 36),
(37, 7, 'Advanced Computer Science', 37),
(38, 7, 'Computer Science Project I', 38),
(39, 7, 'Computer Science Project II', 39),
(40, 7, 'Data Structure and  Algorithms', 40),
(41, 7, 'Design & Analysis of Algorithm', 41),
(42, 7, 'Excel & Access for Business MA', 42),
(43, 7, 'Excel for Business Managers', 43),
(44, 7, 'Introduction to Computer Administration', 44),
(45, 7, 'Introduction to Computing', 45),
(46, 7, 'Introduction to Programming', 46),
(47, 7, 'Object Oriented Programming Techniques', 47),
(48, 7, 'Operating Systems', 48),
(49, 7, 'System Programming', 49),
(50, 7, 'Networking', 50),
(51, 7, 'Parallel Computing', 51),
(52, 8, 'Software Project Management', 52),
(53, 8, 'Software Configuration Management', 53),
(54, 8, 'System Design', 54),
(55, 8, 'Data Analysis and Engineering', 55),
(56, 8, 'Artificial Techniques', 56),
(57, 8, 'Business Development', 57),
(58, 8, 'Probability and Statistics', 58),
(59, 8, 'Programming Languages', 59),
(60, 9, 'Databases & DBMS', 60),
(61, 9, 'Grid / Cloud Computing', 61),
(62, 9, 'Data / Text / Web Mining & Knowledge Discovery', 62),
(63, 9, 'MIS', 63),
(64, 9, 'Information Extraction / Integration', 64),
(65, 9, 'Big Data Analytics', 65),
(66, 9, 'Social Media / Network Analysis', 66),
(67, 10, 'Communication Skills', 67),
(68, 10, 'Management Information Systems Design', 68),
(69, 10, 'E-Public Relations Strategy Design', 69),
(70, 10, 'Persuasion for Technology Management', 70),
(71, 10, 'I.T Change Management', 71),
(72, 10, 'Technology Acceptance Strategy Design', 72),
(73, 10, 'Ability to capitalize on Information Technology background', 73),
(74, 11, 'Advanced Computer Networks', 74),
(75, 11, 'Computer Network Security', 75),
(76, 11, 'Computer Communication & Network', 76),
(77, 11, 'Data Communication and Networking', 77),
(78, 11, 'Data Structures and Algorithms', 78),
(79, 11, 'Excel & Access for Business Management', 79),
(80, 11, 'Introduction to Computer Administration', 80),
(81, 11, 'Introduction to Computer Application', 81),
(82, 11, 'Introduction to Computing', 82),
(83, 11, 'Introduction to Programming', 83),
(84, 11, 'Object Oriented Programming Techniques', 84),
(86, 12, 'So far the most frequent and favorite course of Dr. Humera Tariqâ€™s teaching career is Computer Graphics at undergraduate level preferably with semester project.  The semester projects of computer graphics includes Open GL based 2D application and a  3D virtual environment. The 2D project usually involves Sprite Animation with compulsory Computer Graphics Features i.e. Geometrical Modeling, Animation, Collision Detection, Rendering and Special Effects while 3D project is based on built-in 3D primitives and modeled meshes. Fly By & Walk through, visual Realism techniques like Lighting, Texturing, Shadowing, Ray tracing  are regular features for 3D project. The other frequent teaching courses include Object Oriented Programming, Discrete Mathematics, Modeling and Simulation and Computational Linear Algebra.', 86),
(87, 13, 'Programming', 87),
(88, 13, 'Web Development', 88),
(89, 13, 'Database', 89),
(90, 13, 'Object Oriented Design', 90),
(91, 13, 'Machine Learning', 91),
(92, 13, 'Data Science', 92),
(93, 13, 'Big Data', 93),
(94, 13, 'Artificial Intelligence', 94),
(95, 14, 'Software Development', 95),
(96, 14, 'Compiler Construction', 96),
(97, 14, 'Object Oriented Concept', 97),
(98, 14, 'Database', 98),
(99, 14, 'Programming', 99),
(100, 15, 'Object Oriented Programming.', 100),
(101, 15, 'Automata Theory.', 101),
(102, 15, 'Numerical Analysis.', 102),
(103, 15, 'Computational Linear Algebra,', 103),
(104, 16, 'Computer Architecture and Organization', 104),
(105, 17, 'Software engineering and development ', 105),
(106, 17, 'Software project management ', 106),
(107, 17, 'Database (SQL and Oracle) ', 107),
(108, 17, 'Programming language ', 108),
(109, 17, 'Numerical Analysis', 109),
(110, 18, 'Operating System', 110),
(111, 18, 'Programming Languages', 111),
(112, 18, 'Electronics', 112);

--
-- Truncate table before insert `messages`
--

TRUNCATE TABLE `messages`;
--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`ID`, `Name`, `Email`, `Subject`, `Message`, `SentDate`) VALUES
(1, 'a', 'i@z.com', 'sad', 'sda', '2017-12-30 12:27:50'),
(2, '', '', '1231', '123', '2017-12-30 16:01:53'),
(3, '', '', '123', '123', '2017-12-30 16:03:02');

--
-- Truncate table before insert `news`
--

TRUNCATE TABLE `news`;
--
-- Dumping data for table `news`
--

INSERT INTO `news` (`ID`, `Title`, `Description`, `Image`, `CreatedDate`) VALUES
(1, 'News 1 - Event about to happen', 'This is some text of the news number 1', 'na', '2018-02-11 21:28:03'),
(2, 'News 2 - Some important information', 'This is some text of the news number 2', 'na', '2018-02-11 21:28:20');

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `Username`, `Password`) VALUES
(1, '1', 'c4ca4238a0b923820dcc509a6f75849b'),
(2, 'master', 'eb0a191797624dd3a48fa681d3061212');
SET FOREIGN_KEY_CHECKS=1;
