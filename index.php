<!DOCTYPE html>
<html lang="en" class="full-height">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Home – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<style>
	@media (max-width: 740px) {
		.full-height,
		.full-height body,
		.full-height header,
		.full-height header .view {
			height: 700px; 
		} 
	}
	@media (max-width: 767px) {
		.streak.streak-long-2 {
			height: 700px;
		}
	}
</style>
</head>
<body class="university">
	<header>
		<?php include 'nav.php'; ?>

		<?php
		
		require_once 'admin/functions.php';

		$News = Search_Query("SELECT * from news order by CreatedDate");
		$Events = Search_Query("SELECT * from events order by CreatedDate limit 5");

		?>

		<section class="view" id="home">
			<div id="home" class="view hm-black-strong-1 jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/main1.jpg');">
				<div class="full-bg-img">
					<div class="container flex-center">
						<ul>
							<li>
								<div class="row smooth-scroll">
									<div class="col-md-12 white-text text-center">
										<div class="wow fadeInDown" data-wow-delay="0.2s">
											<h2 class="display-3 font-bold mb-2">Department Of Computer Science UBIT</h2>
											<hr class="hr-light w-50">
											<h3 class="subtext-header mt-4 mb-5">Get in touch with us. If you have any questions regarding programs,</br> facilities or suggestions to make, feel free to  to contact us.</h3>
										</div>
									</div>
								</div>
							</li>
							<li class="mt-5 hidden-xs-down">
								<div id="carouselExampleIndicators" data-interval="3000" class="carousel slide wow fadeInDown text-white" data-ride="carousel">
									<div class="carousel-inner" role="listbox">
										<div class="carousel-item active">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Qualified Teachers</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Peaceful Environment</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Centralized Air Conditioning Building</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
										<div class="carousel-item">
											<ul>
												<li>
													<h1 class="h1-responsive wow fadeInDown uppercase" data-wow-delay="0.2s">Standby Generator</h1>
												</li>
												<li><br><br></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul>						
					</div>
				</div>
			</div>
		</section>

		<div class="foote1r-copyright flex-center" style="height: 50px; font-size: 1.5rem; background-color: #a62c23">
			<marquee class="white-text"><span class="mx-5"><b>
				<?php
				foreach ($News as $Key => $New)
				{
					echo '<span class="mx-5"><b><small>'.date('F j, Y', strtotime($New['CreatedDate'])).':</small></b> '.$New['Title'].'</span>';
					echo ($Key < count($News) - 1 ? '<i class="fa fa-certificate" aria-hidden="true"></i>' : '');
				}
				?>
				
			</marquee>
		</div>
	</header>

	<main>
		<section id="about" class="section pb-4" style="margin-top: 50px">
			<div class="container py-3">
				<div class="divider-new mb-2">
					<h2 class="text-center font-up font-bold wow fadeIn">ABOUT US</h2>
				</div>
				<div class="wow fadeIn" data-wow-delay="0.2s">
					<p align="justify">
						<span>The Department of Computer Science, University of Karachi, was established by a resolution of Academic Council in its meeting, held on November 27. 1984, and it began functioning in the academic year 1985-86 by offering a Degree Program in Master of Computer Science (MCS) and become one of first institutions in Karachi imparting education in Computer Science and Technology. The Department also offers evening program leading to Post Graduate Diploma (PGD) in Computer &amp; Information Sciences. In the year 1995; Department started MCS evening program, on self-finance basis, to cater the growing demand of professionally skilled manpower in the field of Computer Science.</span>
					</p>
				</div>
			</div>
		</section>

		<section id="about" class="section grey lighten-4 pb-5" style="border-top: 2px solid #ddd">
			<div class="container py-3">
				<div class="divider-new mb-0">
					<h2 class="text-center font-up font-bold wow fadeIn" data-wow-delay="0.2s">Chairman Ubit</h2>
				</div>
				<p class="text-center font-up pb-3 font-bold wow fadeIn" data-wow-delay="0.2s">Dr. Sadiq Ali Khan</p>
				<div class="row wow fadeIn px-md-5" data-wow-delay="0.2s">
					<div class="col-12 order-sm-6 col-sm-5 col-lg-3 text-center mb-4 mb-sm-0">
						<img src="images/chairman.png" style="max-width: 300px" class="img-fluid">
					</div>
					<div class="col-12 col-sm-7 col-lg-9">
						<div class="flex-center">
							<div class="text-center">
								<h2 class="h2-responsive mb-5">
									<i class="fa fa-quote-left" aria-hidden="true"></i> I wish best of luck to all those who wish to join Department of Computer Science for further studies and ensure that merit is given the first priority at Department of Computer Science. <i class="fa fa-quote-right" aria-hidden="true"></i>
								</h2>
								<h5 class="text-center font-italic " data-wow-delay="0.2s">~ Dr. Sadiq Ali Khan</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="streak streak-photo streak-long-2 hm-black-strong" style="background-image:url('images/facts.jpg')">
			<div class="mask flex-center">
				<div class="container">
					<h3 class="text-center text-white mb-5 font-up font-bold wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">
						Facts &amp; Figures
					</h3>
					<div class="row text-white text-center wow fadeIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;">
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">9.75</h1>
							<p>Plot Area (Acres)</p>
						</div>
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">+50</h1>
							<p>Number of Teachers</p>
						</div>
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">+2000</h1>
							<p>Number of Students</p>
						</div>
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">3</h1>
							<p>Number of Floors</p>
						</div>
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">+10</h1>
							<p>Class rooms</p>
						</div>
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">+8</h1>
							<p>Computer Labs</p>
						</div>
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">+15</h1>
							<p>Faculty Rooms</p>
						</div>
						<div class="col-md-3 col-6 mb-3">
							<h1 class="green-text mb-1 font-bold">+20</h1>
							<p>Miscellaneous Rooms</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section id="about" class="section pb-4">
			<div class="container py-3">
				<div class="row">
					<div class="col-md-6">
						<div class="divider-new mb-2">
							<h2 class="text-center font-up font-bold wow fadeIn" data-wow-delay="0.2s">News</h2>
						</div>
						<?php 
						foreach ($News as $Key => $New)
						{ 
							if ($Key == 5)
								break;

							echo '<div class="wow fadeIn px-3" data-wow-delay="0.2s">';
							echo '<p class="mb-0 text-primary">'.date('F j, Y, g:i a', strtotime($New['CreatedDate'])).'</p>';
							echo '<p>'.$New['Title'].'</p>';
							echo '</div>';
							echo ($Key < count($News) - 1 ? '<hr>' : '');
						}
						?>
					</div>
					<div class="col-md-6">
						<div class="divider-new mb-2">
							<h2 class="text-center font-up font-bold wow fadeIn" data-wow-delay="0.2s">Events</h2>
						</div>
						<?php 
						foreach ($Events as $Key => $Event)
						{ 
							echo '<div class="wow fadeIn px-3" data-wow-delay="0.2s">';
							echo '<p class="mb-0 text-primary">'.date('F j, Y, g:i a', strtotime($Event['CreatedDate'])).'</p>';
							echo '<p>'.$Event['Title'].'</p>';
							echo '</div>';
							echo ($Key < count($Events) - 1 ? '<hr>' : '');
						}
						?>
					</div>
				</div>
			</div>
		</section>

		<section id="subjects" class="section grey lighten-4 pb-4" style="border-top: 2px solid #ddd; border-bottom: 2px solid #ddd;">
			<div class="container py-3">
				<div class="divider-new mb-4">
					<h2 class="text-center font-up font-bold wow fadeIn">Programs</h2>
				</div>
				<div class="row wow fadeIn" data-wow-delay="0.4s">
					<div class="col-md-2 mb-3 flex-center">
						<img src="images/programs/bscs.png" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>B.S. (Computer Science) (Morning / Evening)</h4>
						<p class="grey-text-555">The BS (CS) is a four years professional degree program consisting of the core courses in science, mathematics, programming and computing with specialization courses.</p>
					</div>
					<div class="col-md-2 mb-3 flex-center">
						<img src="images/programs/bsse.png" class="img-fluid  z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>B.S. (Software Engineering) (Morning / Evening)</h4>
						<p class="grey-text-555">The Bachelor of Science degree in Software Engineering (BSSE) goes beyond programming to include engineering methodologies and hands-on project experience.</p>
					</div>
				</div>
				<div class="row mt-3 wow fadeIn" data-wow-delay="0.4s">
					<div class="col-md-2 mb-3 flex-center">
						<img src="images/programs/mscs.png" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>M.S. / <Ph class="D "></Ph></h4>
						<p class="grey-text-555">The program for the Master of Science in Computer Science (MSCS) prepares students for more highly productive careers in industry.</p>
					</div>
					<div class="col-md-2 mb-3 flex-center">
						<img src="images/programs/phd.png" class="img-fluid z-depth-1 rounded-circle" style="max-width: 160px">
					</div>
					<div class="col-md-4 text-center text-md-left mb-3">
						<h4>M.C.S.</h4>
						<p class="grey-text-555">The MCS is a Professional Master's degree program designed for: Computer science professionals currently working in business who want to advance their careers.</p>
					</div>
				</div>
			</div>
		</section>

		<section id="contact" class="section mb-2">
			<div class="container py-3">
				<div class="divider-new mb-4">
					<h2 class="text-center font-up font-bold wow fadeIn">Contact Us</h2>
				</div>			
				<div class="row wow fadeIn" data-wow-delay="0.4s">
					<div class="col-md-8">
						<form method="post" id="form-contact-us">
							<div class="row">
								<div class="col-md-6">
									<div class="md-form">
										<div class="md-form">
											<input type="text" name="name" id="name" class="form-control">
											<label for="name" class="">Your name</label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="md-form">
										<div class="md-form">
											<input type="email" name="email" id="email" class="form-control">
											<label for="email" class="">Your email</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="md-form">
										<input type="text" name="subject" id="subject" class="form-control" required>
										<label for="subject" class="">Subject</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="md-form">
										<textarea type="text" name="message" id="message" class="md-textarea" required></textarea>
										<label for="message">Your message</label>
									</div>
								</div>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-rounded btn-outline-black waves-effect" id="btn-send-message"><i class="fa fa-send mr-2"></i> Send</button>
							</div>
						</form>
					</div>
					<div class="col-md-4 mt-4 mt-md-0">
						<div class="row">
							<div class="col-12 col-sm-4 col-md-12">
								<ul class="contact-icons">
									<li><i class="fa fa-map-marker fa-2x"></i>
										<p class="grey-text-555">UBIT, University of Karachi</p>
									</li>
								</ul>
							</div>
							<div class="col-12 col-sm-4 col-md-12">
								<ul class="contact-icons">
									<li><i class="fa fa-phone fa-2x"></i>
										<p class="grey-text-555">(+93) 333 222 1111</p>
									</li>
								</ul>
							</div>
							<div class="col-12 col-sm-4 col-md-12">
								<ul class="contact-icons">
									<li><i class="fa fa-envelope fa-2x"></i>
										<p class="grey-text-555">info@ubit.com</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script type="text/javascript" src="js/jquery.particleground.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$("a").on('click', function(event) {
				if (this.hash !== "") {
					var hash = this.hash;
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 1000, function(){      	
						window.location.hash = hash;
					});
				}
			});

			$("#form-contact-us").submit(function(e) {
				e.preventDefault();
				$("#btn-send-message").prop('disabled', 'disabled')
				$("#btn-send-message").html('<i class="fa fa-spinner fa-spin mr-2"></i> Sending');
				var formData = new FormData(this);
				$.ajax({
					type: "POST",
					url: "sendmessage.php",
					data: formData, 
					cache: false,
					contentType: false,
					processData: false,
					success: function (data) {
						if (data === 'true')
						{
							toastr.success('Message sent successfully', '', {positionClass: 'toast-bottom-left'});
							$("#form-contact-us")[0].reset();
						}
						else
						{
							toastr.error(data, '', {positionClass: 'toast-bottom-left'});
						}
						$("#btn-send-message").prop('disabled', '')
						$("#btn-send-message").html('<i class="fa fa-send mr-2"></i> Send');
					}
				});
			});
		});
		$("#particles").particleground({
			minSpeedX: .6,
			minSpeedY: .6,
			dotColor: "#ffffff",
			lineColor: "#ffffff",
			density: 8e3,
			particleRadius: 3,
			parallaxMultiplier: 5.2,
			proximity: 0
		});
	</script>
</body>
</html>