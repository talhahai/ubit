<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>GPA Chart – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Grade Point Table</h1>
				<h5>Programs</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="bscs.php" class="list-group-item grey lighten-4">B.S.C.S.</a>
						<a href="bsse.php" class="list-group-item grey lighten-4">B.S.S.E.</a>
						<a href="mcs.php" class="list-group-item grey lighten-4">M.C.S.</a>
						<a href="ms_phd.php" class="list-group-item grey lighten-4">M.S. / Ph.D.</a>
						<a href="gradepoint.php" class="list-group-item active">Grade Point Table</a>
						<ul class="nav flex-column smooth-scroll-custom" id="nav-scrollspy" role="navigation">
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#table" role="tab">Grade Point Table</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#assessment" role="tab">Assessment &amp; Categories</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color" id="table">Grade Point Table</h4>
					<p>Following is the table for Numeric &amp; Alphabetical grades with Grade Point &amp; CGPR formula for B.S. (Four Years) degree program:</p>
					
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 33%">Numeric Score</th>
								<th class="text-center font-weight-bold" style="width: 33%">Alphabetic Grade</th>
								<th class="text-center font-weight-bold" style="width: 33%">Grade Point</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>90 &amp; above</td>
								<td>A+</td>
								<td>4.0</td>
							</tr>
							<tr>
								<td>85-89</td>
								<td>A</td>
								<td>4.0</td>
							</tr>
							<tr>
								<td>80-84</td>
								<td>A-</td>
								<td>3.8</td>
							</tr>
							<tr>
								<td>75-79</td>
								<td>B+</td>
								<td>3.4</td>
							</tr>
							<tr>
								<td>71-74</td>
								<td>B</td>
								<td>3.0</td>
							</tr>
							<tr>
								<td>68-70</td>
								<td>B-</td>
								<td>2.8</td>
							</tr>
							<tr>
								<td>64-67</td>
								<td>C+</td>
								<td>2.4</td>
							</tr>
							<tr>
								<td>61-63</td>
								<td>C</td>
								<td>2.0</td>
							</tr>
							<tr>
								<td>57-60</td>
								<td>C-</td>
								<td>1.8</td>
							</tr>
							<tr>
								<td>53-56</td>
								<td>D+</td>
								<td>1.4</td>
							</tr>
							<tr>
								<td>50-52</td>
								<td>D</td>
								<td>1.0</td>
							</tr>
							<tr>
								<td>Below</td>
								<td>Fails</td>
								<td>0.0</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color mt-4" id="assessment">Assessment</h5>
					<p>A minimum of 50% marks are required to pass. Both theory and practical exams are separate passing heads.</p>
					
					<h4 class="green-color mt-4">Categories</h4>
					<ul>
						<li><span class="font-weight-bold">Category K: </span> Applicants who passed their prerequisite examination from Karachi Board. Also those who passed their prerequisite examination from the Federal Board provided that the college where studied is located in Karachi.</li>
						<li><span class="font-weight-bold">Category S: </span> Applicants who passed their prerequisite examination from any Board located in any where in the Province of Sindh excluding Karachi.</li>						
						<li><span class="font-weight-bold">Category P: </span> Applicants who passed their prerequisite examination from Pakistan or foreign Board/Teaching Institute excluding those located in the Provice of Sindh.</li>						
					</ul>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>