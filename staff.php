<?php

if (!isset($_GET['id']))
{
	header('location: faculty.php');
}

require_once 'admin/functions.php';

$Faculties = Search_Query("SELECT *, 
	(select count(1) from educations where facultyid = f.id) as 'EducationCount',
	(select count(1) from awards where facultyid = f.id) as 'AwardCount',
	(select count(1) from interests where facultyid = f.id) as 'InterestCount'
	from faculty f order by priority");
$Data = Search_Query("SELECT * from faculty where ID = '".$_GET['id']."'");
if (empty($Data))
{
	header('location: faculty.php');
}
else
{
	$Data = $Data[0];
}
$Educations = Search_Query("SELECT * from educations where FacultyID = '".$_GET['id']."' order by priority");
$Awards = Search_Query("SELECT * from awards where FacultyID = '".$_GET['id']."' order by priority");
$Interests = Search_Query("SELECT * from interests where FacultyID = '".$_GET['id']."' order by priority");

if ($Data['Biography'] == '' && empty($Educations) && empty($Awards) && empty($Interests))
{
	header('location: faculty.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title><?php echo $Data['Name'] ?> – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2"><?php echo $Data['Name'].''.($Data['Degrees'] != "" ? '; '.$Data['Degrees'] : '') ?></h1>
				<h5><?php echo $Data['Designation'] ?></h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links">
						<?php
						foreach ($Faculties as $Faculty)
						{
							$Url = 'staff.php?id='.$Faculty['ID'];
							if ($Faculty['Biography'] == '' && $Faculty['EducationCount'] == 0 &&
								$Faculty['AwardCount'] == 0 && $Faculty['InterestCount'] == 0)
							{
								$Url = '#';
							}

							if ($Faculty['ID'] == $_GET['id'])
							{
								echo '<a href="'.$Url.'" class="list-group-item active">'.$Faculty['Name'].'</a>';
							}
							else
							{
								echo '<a href="'.$Url.'" class="list-group-item grey lighten-4">'.$Faculty['Name'].'</a>';
							}
						}
						?>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<div class="d-inline-sm-up pull-right-sm-up text-center ml-sm-4 mb-3" style="max-width: 300px">
						<img src="<?php echo $Data['Image'] ?>" width="300" class="img-fluid mb-3">
						<?php 
						if ($Data['Quote'] != '')
						{ 
							echo '<h5 class="mb-1"><i class="fa fa-quote-left" aria-hidden="true"></i> '.$Data['Quote'].' <i class="fa fa-quote-right" aria-hidden="true"></i></h5>';
							echo '<p class="mb-0 font-italic">~ '.$Data['Name'].'</p>';
						}
						if ($Data['Email'] != '')
						{
							echo '<h6 class="mt-3"><i class="fa fa-envelope" aria-hidden="true"></i> '.$Data['Email'].'</h6>';
						}
						?>
					</div>
					<?php 
					if ($Data['Biography'] != '')
					{ 
						echo '<h4 class="green-color">Inroduction</h4>';
						echo '<p>'.nl2br($Data['Biography']).'</p>';
					}
					if (!empty($Educations))
					{
						echo '<h4 class="green-color mt-4">Education</h4>';
						echo '<ul>';
						foreach ($Educations as $Education)
						{
							echo '<li>'.$Education['Description'].'</li>';
						}
						echo '</ul>';
					}
					if (!empty($Awards))
					{
						echo '<h4 class="green-color mt-4">Awards</h4>';
						echo '<ul>';
						foreach ($Awards as $Award)
						{
							echo '<li>'.$Award['Description'].'</li>';
						}
						echo '</ul>';
					}
					if (!empty($Interests))
					{
						echo '<h4 class="green-color mt-4">Areas of Expertise/Interest</h4>';
						echo '<ul>';
						foreach ($Interests as $Interest)
						{
							echo '<li>'.$Interest['Description'].'</li>';
						}
						echo '</ul>';
					}
					?>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();
		});
	</script>
</body>
</html>