<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Affiliated Colleges – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Affiliated Colleges</h1>
				<h5>Admissions</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="why_ubit.php" class="list-group-item grey lighten-4">Why Choose UBIT</a>
						<a href="affiliated_colleges.php" class="list-group-item active">Affiliated Colleges</a>
						<a href="fee_structure.php" class="list-group-item grey lighten-4">Fees Structure</a>
						<a href="eligibility_criteria.php" class="list-group-item grey lighten-4">Eligibility Criteria</a>
						<a href="admission_policy.php" class="list-group-item grey lighten-4">Admission Policy</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color">Affiliated Colleges</h4>
					<table class="table table-bordered course-table">
						<thead>
							<th>Sr. No</th>
							<th>Name of Institutes</th>
							<th>Date of Affiliation</th>
							<th>Phone No.</th>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Crescent Degree Science and Commerce College Plot#: 8 to 12. Sector 4-B. Saeedabad. Baldia Town. Karachi</td>
								<td>Sept 29, 2010</td>
								<td>021-2815395. 2813846. 2815307</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Orasoft Training Institute. Karachi 3rd Floor SB-39. Block-13-B. Gulshan-e-Iqbal. Karachi</td>
								<td>Sept 29, 2010</td>
								<td>021-34823725-26 021-34993263 0333-2130582</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Abacus Institute of Management and Computer Science. Karachi. C-7/A K.D.A. Officers Co-operative Housing Society. Opp: P.N.S. Bahadur. Stadium. Dalmia Road. Karachi</td>
								<td>Sept 29, 2010</td>
								<td>021-34985951 021-34822512</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Indus Public College. Karachi B-41/1. 41/ 2. Block 4-A. Mobinah Town Police Station. Abul Hassan Asphani Road. Shafi Colony. Gulshan-e-Iqbal. Karachi</td>
								<td>Sept 29, 2010</td>
								<td>021-34961721 021-34974228</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Al-Mezan Commerce College. 16-AA. Majistrate Colony. Satellite Town. Rawalpindi</td>
								<td>Sept 29, 2010</td>
								<td>051-4329351</td>
							</tr>
							<tr>
								<td>6</td>
								<td>Sir Syed Science College Tipu Road. Post Office. Chaklala 4600 Rawalpindi. </td>
								<td>Sept 29, 2010</td>
								<td>051-5763106 051-5591525</td>
							</tr>
							<tr>
								<td>7</td>
								<td>Central Group of Colleges 58-A. Gulberg-III. Lahore.</td>
								<td>Sept 29, 2010</td>
								<td>042-35841071</td>
							</tr>
							<tr>
								<td>8</td>
								<td>National Textile College and Management Institute. 5/68 Usman Block. New Garden Town. Lahore</td>
								<td>Sept 29, 2010</td>
								<td>042-35844095-7</td>
							</tr>
							<tr>
								<td>9</td>
								<td>Potohar College of Science and Commerce Choudhry Plaza. Mureed Chowk. Dubaran Road. Kaller Syedan District. Rawalpindi</td>
								<td>Sept 29, 2010</td>
								<td>051-3570810 051-3570833</td>
							</tr>
							<tr>
								<td>10</td>
								<td>Wah College of Accountency CB-15. Street# 1. Gudwal Mor. Beside Chicken Hut. The Mall. Wah Cantt</td>
								<td>Sept 29, 2010</td>
								<td>051-4328024. 051-4541429</td>
							</tr>
							<tr>
								<td>11</td>
								<td>Allama Iqbal College Opp: Coca Cola Agency. Jail Road. Pakpura. Sialkot</td>
								<td>Sept 29, 2010</td>
								<td>0344-6350211</td>
							</tr>
							<tr>
								<td>12</td>
								<td>Farabi College of Commerce 9 Green Town. Lahore Road. Sheikhupura.</td>
								<td>Sept 29, 2010</td>
								<td>056-3792027</td>
							</tr>
							<tr>
								<td>13</td>
								<td>IMIT Group of Colleges 108-A. Social Security Building. Jarawala Road. Faisalabad.</td>
								<td>Sept 29, 2010</td>
								<td>041-8733474. 8555797</td>
							</tr>
							<tr>
								<td>14</td>
								<td>Chenab College of Advance Studies 26-A. Batala Colony. Faisalabad</td>
								<td>Sept 29, 2010</td>
								<td>041-8545655</td>
							</tr>
							<tr>
								<td>15</td>
								<td>Kingston College of Science &amp; Commerce 126-C. Peoples Colony# 1. Faisalabad</td>
								<td>Sept 29, 2010</td>
								<td>041-8716987. 041-2022864</td>
							</tr>
							<tr>
								<td>16</td>
								<td>M. A. Jinnah College 94-C. Peoples Colony# 1. Faisalabad</td>
								<td>Sept 29, 2010</td>
								<td>041-8545577</td>
							</tr>
							<tr>
								<td>17</td>
								<td>Al-Habib College 35/C-1. Peoples Colony No. 1. Near Hameed Chok. Faisalabad</td>
								<td>Sept 29, 2010</td>
								<td>041-855788. 041-855799. 041-8555899 0300-8665554</td>
							</tr>
							<tr>
								<td>18</td>
								<td>Alama Iqbal College of Commerce .BhawalPur Near D.C. Office Bhawalpur.</td>
								<td>Sept 29, 2010</td>
								<td>0853-363299,0301-3690422,0333-7895356</td>
							</tr>
							<tr>
								<td>19</td>
								<td>Turbat College of education District Katch.Turbat</td>
								<td>Sept 29, 2010</td>
								<td>0852-411582,0332-2857712</td>
							</tr>
							<tr>
								<td>20</td>
								<td>Ayans College of Education (for women) Main RCD Road. Zehri Street. Hub. Balochistan</td>
								<td>Sept 29, 2010</td>
								<td>0853-310113</td>
							</tr>
							<tr>
								<td>21</td>
								<td>Indus International Institute. 2 K.M jampur Road D.G Khan.</td>
								<td>Sept 29, 2010</td>
								<td>064-2004300 0313-6733505</td>
							</tr>
							<tr>
								<td>22</td>
								<td>East &amp; West education System 35 Islamabad.</td>
								<td>Sept 29, 2010</td>
								<td>051-2279191 051-2279192 0333-3941249 0314-5100550</td>
							</tr>
							<tr>
								<td>23</td>
								<td>College of Excellency Latif Abad No 6 Hyderabad.</td>
								<td>Sept 29, 2010</td>
								<td>0300-8213621 3644161139</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>