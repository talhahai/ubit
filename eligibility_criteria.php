<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Eligibility Criteria – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Eligibility Criteria</h1>
				<h5>Admissions</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="why_ubit.php" class="list-group-item grey lighten-4">Why Choose UBIT</a>
						<a href="affiliated_colleges.php" class="list-group-item grey lighten-4">Affiliated Colleges</a>
						<a href="fee_structure.php" class="list-group-item grey lighten-4">Fees Structure</a>
						<a href="admission_policy.php" class="list-group-item grey lighten-4">Admission Policy</a>
						<a href="eligibility_criteria.php" class="list-group-item active">Eligibility Criteria</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color">Eligibility Criteria</h4>
					<ul>
						<li>Applicant for admission MUST MEET THE ELIGIBILITY REQUIREMENTS set-forth by UBIT.</li>
						<li>Candidates are advised to CONFIRM THEIR ELIGIBILITY prior apply.</li>
						<li>In case of annual system, eligibility will be determined on the basis of result in percentages.</li>
						<li>In case of semester system, eligibility will be determined on the basis of CGPA obtained out of 4.00.</li>
						<li>In case the result is shown both in CGPA and percentage, CGPA will be considered.</li>
					</ul>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js">
	</script>
	<script type="text/javascript" src="js/popper.min.js">
	</script>
	<script type="text/javascript" src="js/bootstrap.min.js">
	</script>
	<script type="text/javascript" src="js/mdb.min.js">
	</script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>