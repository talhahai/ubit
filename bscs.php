<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>B.S.C.S. – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">B.S. Computer Science</h1>
				<h5>Programs</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="bscs.php" class="list-group-item active">B.S.C.S.</a>
						<ul class="nav flex-column smooth-scroll-custom" id="nav-scrollspy" role="navigation">
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#about" role="tab">About B.S.C.S.</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester1" role="tab">Semester 1</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester2" role="tab">Semester 2</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester3" role="tab">Semester 3</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester4" role="tab">Semester 4</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester5" role="tab">Semester 5</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester6" role="tab">Semester 6</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester7" role="tab">Semester 7</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester8" role="tab">Semester 8</a>
							</li>
						</ul>
						<a href="bsse.php" class="list-group-item grey lighten-4">B.S.S.E.</a>
						<a href="mcs.php" class="list-group-item grey lighten-4">M.C.S.</a>
						<a href="ms_phd.php" class="list-group-item grey lighten-4">M.S. / Ph.D.</a>
						<a href="gradepoint.php" class="list-group-item grey lighten-4">Grade Point Table</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color" id="about">B.S. Computer Science (Morning / Evening)</h4>
					<ul>
						<li>Duration of the program, Four years.</li>
						<li>Number of semesters, Eight (Two semester every year)</li>
						<li>Duration of each semester is 16 weeks or so.</li>
						<li>Number of courses, Twelve courses every year (6 courses per semester)</li>
						<li>Credit hours per course, Three credit hours per course</li>
						<li>Total credit hours 144</li>
						<li>Total number of courses 48</li>
					</ul>

					<h4 class="green-color my-4">B.S. (Computer Science) Courses</h4>

					<h5 class="green-color text-center" id="semester1">Semester - I</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-301</td>
								<td>Introduction to Computer Science - I</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-303</td>
								<td>Mathematics - I (Calculus)</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>BSCS-305</td>
								<td>Statistics and Data Analysis</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-307</td>
								<td>Physics - I (General Physics)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-309</td>
								<td>English</td>
								<td>3</td>
							</tr>
							<tr>
								<td>BSCS-311</td>
								<td>Islamic Learning &amp; Pakistan Studies or Ethics &amp; Pakistan Studies</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color text-center" id="semester2">Semester - II</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-302</td>
								<td>Introduction to Computer Science - II</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-304</td>
								<td>Mathematics - II (Differential Equations)</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>BSCS-306</td>
								<td>Probability and Statistical Methods</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS308</td>
								<td>Physics - II (Electricity and Magnetism)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-310</td>
								<td>English</td>
								<td>3</td>
							</tr>
							<tr>
								<td>BSCS-312</td>
								<td>Urdu</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester3">Semester - III</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-401</td>
								<td>Digital Computer Design Fundamentals</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-403</td>
								<td>Assembly Language Programming</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-405</td>
								<td>Mathematics - III (Linear Algebra and Analytical Geometry)</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>BSCS-409</td>
								<td>Materials, Semiconductors and Devices</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-411</td>
								<td>Discrete Mathematics</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>BSCS-413</td>
								<td>Object Oriented Programming</td>
								<td>2+1</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester4">Semester - IV</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-402</td>
								<td>Data Structures</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-404</td>
								<td>System Design with Microprocessors</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-406</td>
								<td>Mathematics - IV (Numerical Computing)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-410</td>
								<td>Electronics</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-412</td>
								<td>Software Engineering &amp; Project Management</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-414</td>
								<td>Communication Skills and Report Writing</td>
								<td>3+0</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester5">Semester - V</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-501</td>
								<td>Theory of Computer Science</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>BSCS-503</td>
								<td>Data Communication and Networking - I</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-505</td>
								<td>Stochastic Processes and Inference (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-507</td>
								<td>Operations Research - I (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-509</td>
								<td>Database Systems</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-511</td>
								<td>Computer Organization and Architecture</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-513</td>
								<td>Advanced Numerical Analysis (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-515</td>
								<td>Artificial Intelligence</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-517</td>
								<td>System Analysis &amp; Design (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-519</td>
								<td>Business Programming Language</td>
								<td>2+1</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester6">Semester - VI</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-502</td>
								<td>Concepts of Operating Systems</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-504</td>
								<td>Compiler Construction - I</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-506</td>
								<td>Modeling and Simulation (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-508</td>
								<td>Operations Research - II (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-510</td>
								<td>Microcomputer Design and Interfacing - I (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-512</td>
								<td>Data Communication and Networking - II</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-514</td>
								<td>Computer Graphics</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-520</td>
								<td>Advanced Software Engineering</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-522</td>
								<td>Expert Systems</td>
								<td>2+1</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester7">Semester - VII</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-601</td>
								<td>Theory of Operating Systems</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-603</td>
								<td>Compiler Construction - II</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-605</td>
								<td>Advanced Computer Graphics (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-607</td>
								<td>Financial Accounting</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>BSCS-609</td>
								<td>Microcomputer Design &amp; Interfacing - II (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-611</td>
								<td>Parallel Computing (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-613</td>
								<td>Management Information System</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-619</td>
								<td>*Thesis</td>
								<td>3</td>
							</tr>
							<tr>
								<td>BSCS-621</td>
								<td>Topics of Current/Special Interest(Optional)</td>
								<td>3</td>
							</tr>
							<tr>
								<td>BSCS-625</td>
								<td>VLSI Design Techniques (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-633</td>
								<td>Internet Application Development (Optional)</td>
								<td>2+1</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester8">Semester - VIII</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>BSCS-602</td>
								<td>Operating System Case Study (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-604</td>
								<td>Natural Language Processing</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-606</td>
								<td>Distributed Database Systems</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-610</td>
								<td>Design and Analysis of Algorithms</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-612</td>
								<td>Financial Management</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>BSCS-616</td>
								<td>Multimedia Systems (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-618</td>
								<td>Computational Linear Algebra (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>BSCS-620</td>
								<td>*Thesis</td>
								<td>3</td>
							</tr>
							<tr>
								<td>BSCS-624</td>
								<td>Project</td>
								<td>0+3</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>