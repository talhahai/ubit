<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>Events – UBIT</title>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
    <?php include_once 'nav.php'; ?>

    <main>
        <div class="mdb-color lighten-5 card-grey-nav flex-center">
            <div class="container"> 
                <h1 class="mb-2">Events</h1>
                <h5>News &amp; Events</h5>
            </div>
        </div>
        <div class="container py-5 grey-text-555">
            <div class="wow fadeIn" data-wow-delay="0.2s">
                <h4 class="green-color mb-4">Events</h4>
                <?php
                require_once 'admin/functions.php';

                $Events = Search_Query("SELECT * from events order by CreatedDate");

                foreach ($Events as $Key => $Event)
                {
                    ?>
                    <div class="card">
                        <div class="card-block p-sm-3">
                            <div class="row">
                                <?php 
                                if ($Event['Image'] != 'na')
                                {
                                    ?>
                                    <div class="col-sm-5 col-md-4 col-lg-3">
                                        <div class="view overlay hm-white-slight rounded z-depth-1-half">
                                            <img src="<?php echo $Event['Image'] ?>" class="img-fluid" style="max-height: 150px; margin: auto;" alt="<?php echo $Event['Title'] ?>">
                                            <a>
                                                <div class="mask waves-effect waves-light"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-7 col-md-8 col-lg-9">
                                        <?php
                                    }
                                    else
                                        echo '<div class="col-12">';
                                    ?>
                                    <div class="p-3 p-sm-0">
                                        <h5 class="font-bold dark-grey-text mb-2">
                                            <?php echo $Event['Title'] ?>
                                        </h5>
                                        <p class="grey-text mb-1"><?php echo $Event['Description'] ?></p>
                                        <p class="mb-1">Event Date: <?php echo $Event['EventDate'] ?></p>
                                        <p class="mb-1">Event Venue: <?php echo $Event['EventVenue'] ?></p>
                                        <p class="mb-0 small">UploadDate: <?php echo date('F j, Y, g:i a', strtotime($Event['CreatedDate'])) ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo ($Key < count($Events) - 1 ? '<hr>' : '') ?>
                    <?php
                }
                ?>
            </div>
        </div>
    </main>

    <?php include 'footer.php'; ?>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <script>
        new WOW().init();
        $(document).ready(function() {
            $('.mdb-select').material_select();
        });
    </script>
</body>
</html>