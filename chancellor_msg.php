<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Chancellor's Message – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Chancellor's Message</h1>
				<h5>About</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links">
						<a href="aboutus.php" class="list-group-item grey lighten-4">About Us</a>
						<a href="vision_mission.php" class="list-group-item grey lighten-4">Vision and Mission</a>
						<a href="chancellor_msg.php" class="list-group-item active">Chancellor's Message</a>
						<a href="vicechancellor_msg.php" class="list-group-item grey lighten-4">Vice Chancellor's Message</a>
						<a href="chairman_msg.php" class="list-group-item grey lighten-4">Chairman's Message</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<div class="d-inline-sm-up pull-right-sm-up text-center ml-sm-4 mb-3" style="max-width: 300px">
						<img src="images/chancellor.jpg" width="300" class="img-fluid mb-3">
						<h5 class="mb-1">Muhammad Zubair</h5>
						<p class="mb-0">Governor of Sindh</p>
					</div>
					<p>On behalf of all the staff members, I would like to welcome everyone to DCS – UBIT. It is a privilege to serve as Chairman of Department of Computer Science – UBIT. Since the birth, DCS – UBIT has gone from strength to strength, proactively engaging with businesses in the development of courses and meeting regional and national needs at a time of constant change and development. As a result we have a reputation for producing graduates that are highly sought after by employers.</p>
					<p>Today DCS – UBIT provides more than 140 courses and the contents of courses offered are revised after every two years and books are recommended to cope with the rapid developments which are taking place in Computer Science and Information Technology. All the curricula have been designed in the light of recommendations of Task Force (on the Curriculum for Computer Science Programs for general Universities of Sindh). Thus all the curricula of Department of Computer Science are developed for CS/IT.</p>
					<p>Excellent career opportunities exist for the graduates of Department of Computer Science. More than 80% of graduates are immediately hired by top ranking software houses and other institutions.</p>
					<p>We hope we can count on you in our ongoing effort to make a better society which is more just and developed; a more sustainable and advanced society that encourages equality among men and women and provides a higher quality of life for citizens.</p>
					<p>I wish best of luck to all those who wish to join Department of Computer Science for further studies and ensure that merit is given the first priority at Department of Computer Science.</p>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();
		});
	</script>
</body>
</html>