-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2017 at 04:02 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ubit`
--
CREATE DATABASE IF NOT EXISTS `ubit` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ubit`;

-- --------------------------------------------------------

--
-- Table structure for table `awards`
--

DROP TABLE IF EXISTS `awards`;
CREATE TABLE IF NOT EXISTS `awards` (
  `ID` int(11) NOT NULL,
  `FacultyID` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `educations`
--

DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `ID` int(11) NOT NULL,
  `FacultyID` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `ID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `EventDate` varchar(255) NOT NULL,
  `EventVenue` varchar(255) NOT NULL,
  `CreatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

DROP TABLE IF EXISTS `faculty`;
CREATE TABLE IF NOT EXISTS `faculty` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Biography` text,
  `Quote` varchar(255) DEFAULT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

DROP TABLE IF EXISTS `interests`;
CREATE TABLE IF NOT EXISTS `interests` (
  `ID` int(11) NOT NULL,
  `FacultyID` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Message` text NOT NULL,
  `SentDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `ID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `CreatedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FacultyID` (`FacultyID`);

--
-- Indexes for table `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FacultyID` (`FacultyID`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FacultyID` (`FacultyID`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `awards`
--
ALTER TABLE `awards`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `educations`
--
ALTER TABLE `educations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `awards`
--
ALTER TABLE `awards`
  ADD CONSTRAINT `awards_facultyid` FOREIGN KEY (`FacultyID`) REFERENCES `faculty` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `educations`
--
ALTER TABLE `educations`
  ADD CONSTRAINT `educations_facultyid` FOREIGN KEY (`FacultyID`) REFERENCES `faculty` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `interests`
--
ALTER TABLE `interests`
  ADD CONSTRAINT `interests_facultyid` FOREIGN KEY (`FacultyID`) REFERENCES `faculty` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `users` (`ID`, `Username`, `Password`) VALUES(1, '1', 'c4ca4238a0b923820dcc509a6f75849b');
INSERT INTO `users` (`ID`, `Username`, `Password`) VALUES(2, 'master', '8eaf2fa908473cdc13e72fe780f02721');

INSERT INTO `faculty` (`ID`, `Name`, `Designation`, `Image`, `Biography`, `Quote`, `Priority`) VALUES(1, 'Prof Dr S.M. Aqil Burney', 'Founder Director', 'images/faculty/1.png', 'Prof Dr S.M. Aqil Burney has served well over 42 years at University of Karachi. Now Dr. Aqil Burney is Professor at College of Computer Science and Information Systems (CCSIS) at Institute of Business Management (IoBM)Karachi, one of the leading Business Schools of the country.\n\nDr. Aqil Burney was a Meritorious Professor (R.E.) and approved supervisor in Computer Science and Statistics by the Higher Education Commission, Govt. of Pakistan. He was also the founder Director (UBIT) & Chairman of the Department of Computer Science, University of Karachi. He is also member of various higher academic boards of different universities of Pakistan.\n\nHis research interest includes artificial intelligence, soft computing, neural networks, fuzzy logic, data mining, statistics, simulation and stochastic modeling of mobile communication system and networks and network security, ,Now heading the detp. of actuarial science and risk management at CSCIS â€“ IoBM.Teaching mostly MS(CS) PhD(CS))courses such as Data Warehousing,data mining & ML and information retrieval systems,fuzzy systems ,advanced theory of statistics, Markov chains and Financial Time Series.\n\nHe is author/co-author of five books, various technical reports and has published more than 145 research papers in national and international research journals and has attended around 60 national and international conferences/seminars/symposiums with more than 470 citations. He has supervised hundreds of projects in operation research, statistics, simulation and modeling, software engineering and intelligent systems,Computer science , stochastic processes and biological/ physical sciences. Twelve research scholars completed Ph.D and five research students completed M Phil / M.S under his supervision in mathematical and computing sciences. At present more than five M.S./Ph.D. scholars are working under his supervision in the field of CS/IT and statistics in CS/IT.', '', 1);
INSERT INTO `faculty` (`ID`, `Name`, `Designation`, `Image`, `Biography`, `Quote`, `Priority`) VALUES(2, 'Dr. M. Sadiq Ali Khan', 'Chairman', 'images/faculty/2.png', 'Dr. M. Sadiq Ali Khan is Chairman at Department of Computer Science â€“ UBIT. He completed his Ph.D in 2011 under the supervision of Prof. Dr. S.M. Aqil Burney.\r\n\r\nDr. M. Sadiq Ali Khan had earned his Bachelors and Masters degree from Sir Syed University of Engineering and Technology. His main areas of interests are Network Security, Computer Networking, Software Engineering, Educational Administration, Security Information Technology', '', 2);
INSERT INTO `faculty` (`ID`, `Name`, `Designation`, `Image`, `Biography`, `Quote`, `Priority`) VALUES(3, 'Mr. Syed Jamal Hussain', 'Assistant Professor', 'images/faculty/3.png', 'Mr. Syed Jamal Hussain is Assistant Professor at Department of Computer and his hard-work for this department can never be neglected. He is one of  the senior teachers who is eager his knowledge and experiences with the students.\r\n\r\nMr. Syed Jamal Hussainâ€™s teaching style is unique. He blends well with the level of students and makes it easier for the students to understand difficult topics.', '', 3);
INSERT INTO `faculty` (`ID`, `Name`, `Designation`, `Image`, `Biography`, `Quote`, `Priority`) VALUES(4, 'Mr. Badar Sami', 'Assistant Professor', 'images/faculty/4.png', 'Mr. Badar Sami is Assistant Professor of Department of Computer Science â€“ UBIT. He is one of the best teachers of the UBIT. According to Mr. Badar Sami, the goal of the education is to have a peaceful, vibrant society, so that we can interact with other people in a peaceful manner.\r\n\r\nMr. Badar Sami is not only the Incharge but the pioneer for students. He has done his Bachelors in Science and then he did Masters in Computer Science from University of Karachi. Software Development and programming are his main fields.\r\n\r\nCurrently he is doing Ph.D in Statistical techniques in Text Mining of information retrieval systems. His Ph.D. is in final stages and is supervised by Prof. Dr. S.M. Aqil Burney', '', 4);

SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
