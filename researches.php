<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Researches – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">Researches</h1>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="wow fadeIn" data-wow-delay="0.2s">
				<h4 class="green-color">Researches</h4>
				<p>The department has produced several Ph.D.s during the past few years. These active researchers are engaged in research activities all the year. As a result, many publications in reputed international journals and conferences are presented each year.</p>
				<table class="table table-bordered course-table">
					<thead>
						<th>Candidate</th>
						<th>Year</th>
						<th>Title</th>
						<th>Status</th>
						<th>Supervisor</th>
					</thead>
					<tbody>
						<tr>
							<td>Maj. Dr. Afzal Saleemi</td>
							<td>2002 (M.Phil. / Ph.D.)</td>
							<td>New trends in smoothing and filtering of temporal data using information technology</td>
							<td>Ph.D.  Awarded (2006)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. Tahseen Jilani</td>
							<td>2003 (M.Phil. / Ph.D.)</td>
							<td>Soft computing applications in Actuarial Science</td>
							<td>Ph.D.  Awarded (2007)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. Nadeem Mahmood</td>
							<td>2003 (M.Phil. / Ph.D.)</td>
							<td>Applications of Temporal Logic in RDBMS</td>
							<td>Ph.D.  Awarded (2010)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Mr. Hussain Saleem</td>
							<td>2003 (M.Phil. / Ph.D.)</td>
							<td>Software configuration management with emphasis on trace dependency analysis</td>
							<td>Final stages</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. Akhtar Raza</td>
							<td>2004 (M.Phil. / Ph.D.)</td>
							<td>Time series models for management of computing infrastructure and resources for forecasting and control</td>
							<td>Ph.D.  awarded (2011)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Mr. Badar Sami</td>
							<td>2004 (M.Phil. / Ph.D.)</td>
							<td>Statistical techniques in Text Mining of information retrieval systems</td>
							<td>Final stages</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. M. Sadiq Ali Khan</td>
							<td>2006 (M.Phil. / Ph.D.)</td>
							<td>Recent trends in computer intrusion detection and network monitoring: A statistical modeling approach</td>
							<td>Ph.D.  Awarded (2011)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Mr. Syed Asim Ali</td>
							<td>2007 (M.Phil. / Ph.D.)</td>
							<td>Developing an Information and Communication Technology Acceptance Model in the context of Social Environment and measuring the Performance using Diffusion Models</td>
							<td>Final stages</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. Syed Asif Ali</td>
							<td>(M.Phil. / Ph.D.)</td>
							<td>Artificial Intelligence techniques in Special Education using Information Technology</td>
							<td>Ph.D.  Awarded (2011)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. M. Zamin Ali Khan</td>
							<td>2008 (Ph.D.)</td>
							<td>Statistical approach to design analog VLSI / low power low noise CMOS amplifier for wireless application</td>
							<td>Ph.D.  Awarded (2011)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. Humera Tariq</td>
							<td>2008 (M.S. / Ph.D.)</td>
							<td> </td>
							<td>Ph.D.  Awarded (2015)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Mr. Zain Abbas</td>
							<td>2008 (M.S. / Ph.D.)</td>
							<td>Intelligent web technologies using Rough sets in context of healthcare information</td>
							<td>Thesis Submitted</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Dr. Qamar-ul-Arifeen</td>
							<td>2008 (M.S. / Ph.D.)</td>
							<td>Soft computing approach in forensics: An intelligent approach</td>
							<td>Ph.D.  Awarded (2015)</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Mr. Jawed Nasim</td>
							<td>2009 (M.S. / Ph.D.)</td>
							<td>Application of ICT in Agricultural science: A Pakistan case study</td>
							<td>In progress</td>
							<td>Prof. Dr. Aqil Burney</td>
						</tr>
						<tr>
							<td>Mr. Farhan Ahmed Siddiqui</td>
							<td>2007(M.Phil. / Ph.D.)</td>
							<td>Grid Computing in Financial services</td>
							<td><p>In progress</td>
							<td>Prof. Dr. Nasir Touheed</td>
						</tr>
						<tr>
							<td>Dr. Muhammad Saeed</td>
							<td>2007(M.Phil. / Ph.D.)</td>
							<td>Improved Data Mining in distributed environments</td>
							<td><p>Ph.D. Awarded (2015)</td>
							<td>Prof. Dr. Nasir Touheed</td>
						</tr>
						<tr>
							<td>Mr. Syed Abdul Khaliq Bari</td>
							<td>2008 (M.S. / Ph.D.)</td>
							<td>Modeling and Design of Databases for Multimedia Content Management</td>
							<td>In progress</td>
							<td>Dr. Nadeem Mahmood</td>
						</tr>
						<tr>
							<td>Mr. Kashif Rizwan</td>
							<td>2009 (M.S. / Ph.D.)</td>
							<td> </td>
							<td>In progress</td>
							<td>Dr. Nadeem Mahmood</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();
		});
	</script>
</body>
</html>