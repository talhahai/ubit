<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Mission &amp; Vision – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<?php include_once 'nav.php'; ?>
	
	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">
				<h1 class="mb-2">Our Vision And Mission</h1>
				<h5>About</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links">
						<a href="aboutus.php" class="list-group-item grey lighten-4">About Us</a>
						<a href="vision_mission.php" class="list-group-item active">Our Vision And Mission</a>
						<a href="chancellor_msg.php" class="list-group-item grey lighten-4">Chancellor's Message</a>
						<a href="vicechancellor_msg.php" class="list-group-item grey lighten-4">Vice Chancellor's Message</a>
						<a href="chairman_msg.php" class="list-group-item grey lighten-4">Chairman's Message</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<p align="justify">The Department of Computer Science mission is to provide a transformative academic experience that:</p>
					<ul>
						<li>Encourages rigorous inquiry</li>
						<li>Enables critical thinking and problem solving skills</li>
						<li>Cultivates knowledge in dynamic learning environments</li>
						<li>Ignites awareness and involvement in our interdependent world</li>
						<li>Requires research and creative activities</li>
						<li>Actively engages in continuous improvement</li>
					</ul>
					<h4 class="green-color">Our Vision</h4>
					<p align="justify">To provide high quality, student-centered education and lifelong learning opportunities for the communities we serve.</p>
					<h4 class="green-color mt-4">Our Mission</h4>
					<p align="justify">The Department of Computer Science – UBIT is dedicated to excellence demonstrated through national and international recognition. Through freedom of academic inquiry and expression, we create and disseminate knowledge by means of scholarly and creative achievements, graduate and professional education, and outreach.</p>
					<p align="justify">With our focus on teaching and learning, the department helps every student grow intellectually and become a contributing member of the state, national, and world communities. Through research, teaching, service, and outreach, we embrace diversity and cultivate leadership, integrity, and engaged citizenship in our students, faculty, staff, and alumni. We promote the health and well-being of citizens by enhancing the social, economic, cultural, and natural environments of the state and beyond.</p>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();
		});
	</script>
</body>
</html>