<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>About Us – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">About Us</h1>
				<h5>About</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links">
						<a href="aboutus.php" class="list-group-item active">About Us</a>
						<a href="vision_mission.php" class="list-group-item grey lighten-4">Vision and Mission</a>
						<a href="chancellor_msg.php" class="list-group-item grey lighten-4">Chancellor's Message</a>
						<a href="vicechancellor_msg.php" class="list-group-item grey lighten-4">Vice Chancellor's Message</a>
						<a href="chairman_msg.php" class="list-group-item grey lighten-4">Chairman's Message</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color">About Us</h4>
					<p align="justify">
						The Department of Computer Science, University of Karachi, was established by a resolution of Academic Council in its meeting, held on November 27. 1984, and it began functioning in the academic year 1985-86 by offering a Degree Program in Master of Computer Science (MCS) and become one of first institutions in Karachi imparting education in Computer Science and Technology.
					</p>
					<p align="justify">
						The Department also offers evening program leading to Post Graduate Diploma (PGD) in Computer & Information Sciences. In the year 1995; Department started MCS evening program, on self-finance basis, to cater the growing demand of professionally skilled manpower in the field of Computer Science.
					</p>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();
		});
	</script>
</body>
</html>