<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>B.S.S.E. – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">B.S. Software Engineering</h1>
				<h5>Programs</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="bscs.php" class="list-group-item grey lighten-4">B.S.C.S.</a>
						<a href="bsse.php" class="list-group-item active">B.S.S.E.</a>
						<ul class="nav flex-column smooth-scroll-custom" id="nav-scrollspy" role="navigation">
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#about" role="tab">About B.S.S.E.</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester1" role="tab">Semester 1</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester2" role="tab">Semester 2</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester3" role="tab">Semester 3</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester4" role="tab">Semester 4</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester5" role="tab">Semester 5</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester6" role="tab">Semester 6</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester7" role="tab">Semester 7</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester8" role="tab">Semester 8</a>
							</li>
						</ul>
						<a href="mcs.php" class="list-group-item grey lighten-4">M.C.S.</a>
						<a href="ms_phd.php" class="list-group-item grey lighten-4">M.S. / Ph.D.</a>
						<a href="gradepoint.php" class="list-group-item grey lighten-4">Grade Point Table</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color" id="about">B.S. Software Engineering (Morning / Evening)</h4>
					<ul>
						<li>Duration of the program, Four years.</li>
						<li>Number of semesters, Eight (Two semester every year)</li>
						<li>Duration of each semester is 16 weeks or so.</li>
						<li>Number of courses, Twelve courses every year (6 courses per semester)</li>
						<li>Credit hours per course, Three credit hours per course</li>
						<li>Total credit hours 144</li>
						<li>Total number of courses 48</li>
					</ul>

					<h4 class="green-color my-4">B.S. (Software Engineering) Courses</h4>

					<h5 class="green-color text-center" id="semester1">Semester - I</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-301</td>
								<td>Calculus and Analytical Geometry - I</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-303</td>
								<td>Computer Logic Design and Computer Organization</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-305</td>
								<td>Introduction to C/C++ Language</td>
								<td>3+1</td>
							</tr>
							<tr>
								<td>CS(SE)-307</td>
								<td>Islamic Studies</td>
								<td>2</td>
							</tr>
							<tr>
								<td>CS(SE)-309</td>
								<td>Probability and Statistics</td>
								<td>3+0</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color text-center" id="semester2">Semester - II</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-302</td>
								<td>Business Communication Skills</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-304</td>
								<td>Pakistan Studies</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-306</td>
								<td>Calculus and Analytical Geometry - II</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-308</td>
								<td>Linear Algebra</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-310</td>
								<td>Object Oriented Concepts using JAVA</td>
								<td>3+1</td>
							</tr>
							<tr>
								<td>CS(SE)-312</td>
								<td>Software Engineering - I</td>
								<td>4</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color text-center" id="semester3">Semester - III</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-401</td>
								<td>Advanced JAVA</td>
								<td>3+1</td>
							</tr>
							<tr>
								<td>CS(SE)-403</td>
								<td>Relational Database Management System (RDBMS)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS(SE)-405</td>
								<td>Data Structure using JAVA</td>
								<td>3+1</td>
							</tr>
							<tr>
								<td>CS(SE)-407</td>
								<td>Operations Research</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-409</td>
								<td>Discrete Mathematics</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-411</td>
								<td>Ordinary Differential Equations</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color text-center" id="semester4">Semester - IV</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-402</td>
								<td>Assembly Language</td>
								<td>3+1</td>
							</tr>
							<tr>
								<td>CS(SE)-404</td>
								<td>Operating Systems</td>
								<td>4</td>
							</tr>
							<tr>
								<td>CS(SE)-406</td>
								<td>Business Economics</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-408</td>
								<td>Numerical Analysis</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-410</td>
								<td>Business Communication Skills - II</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester5">Semester - V</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-501</td>
								<td>Compiler Construction</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-503</td>
								<td>Computer Architecture</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-505</td>
								<td>Organization Behaviour</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-507</td>
								<td>Practical Networking and Data Communication</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>CS(SE)-509</td>
								<td>Software Engineering - II</td>
								<td>4</td>
							</tr>
						</tbody>
					</table>

					<h5 class="green-color text-center" id="semester6">Semester - VI</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-502</td>
								<td>Advance JAVA and Internet Programming</td>
								<td>3+1</td>
							</tr>
							<tr>
								<td>CS(SE)-504</td>
								<td>Focus Course - I</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-506</td>
								<td>Software Project Management</td>
								<td>4</td>
							</tr>
							<tr>
								<td>CS(SE)-508</td>
								<td>Marketing Management</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-510</td>
								<td>Project - I</td>
								<td>0+3</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color text-center" id="semester7">Semester - VII</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-601</td>
								<td>Distributed Computing</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-603</td>
								<td>Software Process Management</td>
								<td>4</td>
							</tr>
							<tr>
								<td>CS(SE)-605</td>
								<td>Computerized Accounting</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-607</td>
								<td>Focus Course - II</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-609</td>
								<td>Software Project + Documentation Skills</td>
								<td>0+4</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color text-center" id="semester8">Semester - VIII</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>CS(SE)-602</td>
								<td>Psychology</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-604</td>
								<td>Software Testing</td>
								<td>2+2</td>
							</tr>
							<tr>
								<td>CS(SE)-606</td>
								<td>Focus Course - III</td>
								<td>3</td>
							</tr>
							<tr>
								<td>CS(SE)-608</td>
								<td>Project - III</td>
								<td>0+4</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>