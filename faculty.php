<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Faculty – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container"> 
				<h1 class="mb-0">Faculty</h1>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="wow fadeIn" data-wow-delay="0.2s">
				<div class="row">
					<?php
					require_once 'admin/functions.php';

					$Faculties = Search_Query("SELECT *, 
						(select count(1) from educations where facultyid = f.id) as 'EducationCount',
						(select count(1) from awards where facultyid = f.id) as 'AwardCount',
						(select count(1) from interests where facultyid = f.id) as 'InterestCount'
						from faculty f order by priority");

					foreach ($Faculties as $Faculty)
					{
						$Url = 'staff.php?id='.$Faculty['ID'];
						if ($Faculty['Biography'] == '' && $Faculty['EducationCount'] == 0 &&
							$Faculty['AwardCount'] == 0 && $Faculty['InterestCount'] == 0)
						{
							$Url = '#';
						}
						?>
						<div class="col-lg-3 col-md-12 my-3">
							<div class="card">
								<div class="view overlay hm-zoom hm-black-strong">
									<img src="<?php echo $Faculty['Image'] ?>" class="img-fluid" alt="photo">
									<a href="<?php echo $Url ?>">
										<div class="mask flex-center waves-effect waves-light">
											<?php echo ($Url == '#' ? '' : '<button type="button" class="btn btn-outline-white waves-effect">View Profile</button>') ?>
										</div>
									</a>
								</div>
								<div class="card-body">
									<p class="card-subtitle subtitle font-italic mb-0"><?php echo $Faculty['Designation'] ?></p>
									<?php 
									if ($Faculty['Degrees'] == "")
										echo '<p class="card-title mb-2"><b><a href="'.$Url.'" class="mdb-color-text">'.$Faculty['Name'].'</a></b></p>';
									else
									{
										echo '<p class="card-title mb-1"><b><a href="'.$Url.'" class="mdb-color-text">'.$Faculty['Name'].'</a></b></p>';
										echo '<p class="card-subtitle subtitle mb-2">'.$Faculty['Degrees'].'</p>';
									}
									?>
									<p class="card-text mb-0" style="font-size: 12px;"><?php echo substr($Faculty['Biography'], 0 ,160).''.(strlen($Faculty['Biography']) > 160 ? '...' : '') ?></p>
								</div>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();
		});
	</script>
</body>
</html>