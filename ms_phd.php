<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>M.S. / Ph.D. – UBIT</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target="#nav-scrollspy">
	<?php include_once 'nav.php'; ?>

	<main>
		<div class="mdb-color lighten-5 card-grey-nav flex-center">
			<div class="container">	
				<h1 class="mb-2">M.S. / Ph.D.</h1>
				<h5>Programs</h5>
			</div>
		</div>
		<div class="container py-5 grey-text-555">
			<div class="row wow fadeIn" data-wow-delay="0.2s">
				<div class="col-md-4 col-lg-3 hidden-sm-down">
					<div class="list-group sidebar-links sticky">
						<a href="bscs.php" class="list-group-item grey lighten-4">B.S.C.S.</a>
						<a href="bsse.php" class="list-group-item grey lighten-4">B.S.S.E.</a>
						<a href="mcs.php" class="list-group-item grey lighten-4">M.C.S.</a>
						<a href="ms_phd.php" class="list-group-item active">M.S. / Ph.D.</a>
						<ul class="nav flex-column smooth-scroll-custom" id="nav-scrollspy" role="navigation">
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#about" role="tab">About M.S. / Ph.D.</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester1" role="tab">M.S. Semester 1</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#semester2" role="tab">M.S. Semester 2</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#confirmationOfAdmission" role="tab">Confirmation Of Admission</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#durationOfStudy" role="tab">Duration Of Study</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#evaluationAndAwardOfMS" role="tab">Evaluation and Award of M.S. Degree</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#conversionToPhD" role="tab">Conversion to Ph.D.</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#evaluationAndAwardOfPhD" role="tab">Evaluation and Award of Ph.D. Degree</a>
							</li>
							<li class="nav-item">
								<a class="nav-link smooth-scroll-link" data-toggle="tab" href="#admissionTest" role="tab">Admission Test</a>
							</li>
						</ul>
						<a href="gradepoint.php" class="list-group-item grey lighten-4">Grade Point Table</a>
					</div>
				</div>
				<div class="col-md-8 col-lg-9">
					<h4 class="green-color" id="about">M.S. Computer Science</h4>
					<p>The B.S. (Computer Science) leads to M.S. (Computer Science) of two year duration with eight full courses each of 3 credit hours.</p>
					
					<ul>
						<li>All selected students will required to complete  8 courses  (3+0 and/or 2+1) of 24 credit hours in two semesters.</li>
						<li>In each semester there will be 4 courses of 3 credit hours (3+0 and/or 2+1) each.</li>
						<li>The student has to pass the course work with CGPA 3.00 or more. In case the student fails to get the desired CGPA, he/she will be allowed to improve the grade. Only one chance for improvement will be given.</li>
						<li>Atleast 75% attendance in the course work is compulsory.</li>
						<li>All selected students will required to complete  8 courses  (3+0 and/or 2+1) of 24 credit hours in two semesters.</li>
					</ul>

					<h4 class="green-color my-4">M.S. (Computer Science) Courses</h4>

					<h5 class="green-color text-center" id="semester1">Semester - I</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>MSCS-701</td>
								<td>Theory of Computation and Logic</td>
								<td>3+0</td>
							</tr>
							<tr>
								<td>MSCS-703</td>
								<td>Advanced Database Systems (ADS)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-705</td>
								<td>Parallel Computing : Hardware Systems (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-707</td>
								<td>Machine Learning Systems (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-709</td>
								<td>Topics in Computer Graphics and Image Analysis (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-711</td>
								<td>Applied Software Project Management (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-713</td>
								<td>Thesis (Optional)</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>
					
					<h5 class="green-color text-center" id="semester2">Semester - II</h5>
					<table class="table table-bordered course-table">
						<thead>
							<tr>
								<th class="text-center font-weight-bold" style="width: 140px">Course Number</th>
								<th class="text-center font-weight-bold">Course Title</th>
								<th class="text-center font-weight-bold" style="width: 140px">Credit Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>MSCS-702</td>
								<td>Advanced Algorithmic Design and Analysis</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-704</td>
								<td>Advanced Data Communication and Networks</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-706</td>
								<td>Parallel Computing : Software (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-708</td>
								<td>Topics in Software Engineering: Formal Methods and Models (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-710</td>
								<td>Bayesian Networks and AI (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-712</td>
								<td>Data Mining (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-714</td>
								<td>Thesis (Optional)</td>
								<td>3</td>
							</tr>
							<tr>
								<td>MSCS-716</td>
								<td>Advanced Topics in AI (Optional)</td>
								<td>2+1</td>
							</tr>
							<tr>
								<td>MSCS-718</td>
								<td>Most Recent Trends and Developments in Software Engineering that are not covered by existing courses. IEEE's Software Engineering Notes, Relevant Periodicals, Journals will be used as course contents. (Optional)</td>
								<td>3</td>
							</tr>
						</tbody>
					</table>

					<p class="mb-0 font-weight-bold">The specific areas and/or courses are:</p>
					<p>Software Engineering, Database, Artificial Intelligence, Multimedia and Graphics, System Engineering, Computer Network and Computer Science education</p>
					
					<h4 class="green-color mt-4" id="confirmationOfAdmission">Confirmation Of Admission</h4>
					<p>After passing the course work with CGPA 3.00, the student will apply through proper channel for confirmation of admission and approval of research title. The student will submit a synopsis along with research topic, bibliography, name of Research Supervisor and a copy of the marks certificate issued by the Semester Examination Section.</p>
					
					<h4 class="green-color mt-4" id="durationOfStudy">Duration Of Study</h4>
					<p class="mb-1">The duration of M.S. or M.S./Ph.D. or Ph.D. program is as under:</p>
					<ul style="list-style: lower-alpha;">
						<li>M.S. Minimum 2 and maximum 5 years from the date of provisional admission.</li>
						<li>Ph.D.
							<ul>
								<li>Minimum 1 and maximum 5 years from the date of conversion from M.S. to Ph.D.;</li>
								<li>Minimum 2 years and maximum 5 years for those admitted directly to Ph.D.</li>
								<li>Students  holding M.Phil  without coursework  (degree received before 2008) will be required  to take  6 credit hours of  PhD  coursework (800 level). Supervisor may recommend additional courses.</li>
							</ul>
						</li>
					</ul>
					
					<h4 class="green-color mt-4" id="evaluationAndAwardOfMS">Evaluation and Award of M.S. Degree</h4>
					<p>The thesis will be evaluated by two experts and the supervisor. The viva voce examination will be conducted after receipt of positive reports from the  experts and their approval by the BASR. On successful defense of the thesis, M.S. degree will be awarded.</p>
					
					<h4 class="green-color mt-4" id="conversionToPhD">Conversion to Ph.D.</h4>
					<p>If the student passes the course work in CGPA 3.0 or more, he/she will be given a   research project by the supervisor which will be processed through BASR. After one year of research, a student may apply for conversion from M.S./Ph.D. to Ph.D. through BASR.</p>
					
					<h4 class="green-color mt-4" id="evaluationAndAwardOfPhD">Evaluation and Award of Ph.D. Degree</h4>
					<ul>
						<li>The student has to take two courses of the relevant subject of 3 credit hours (800 level) each.</li>
						<li>The candidate has to publish at least one research paper in any reputable journal during his /her  research work.</li>
						<li>The thesis will be evaluated by two foreign experts and the supervisor. The viva voce examination will be conducted after receipt of positive reports from the  experts and their approval by the BASR. On successful defense of the thesis, Ph.D. degree will be awarded.</li>
					</ul>
					
					<h4 class="green-color mt-4" id="admissionTest">Admission Test</h4>
					<p>The test comprise of 80 questions from following areas of Computer Science courses and 20 from English and General Knowledge. The areas of Computer Science includes the following subjects</p>
					<ul>
						<li>Artificial Intelligence</li>
						<li>Database</li>
						<li>Theory of Computer Science and Automata</li>
						<li>Computer Architecture</li>
						<li>Operating System</li>
						<li>Data Structure</li>
						<li>Algorithm analysis and Design</li>
						<li>Software Engineering</li>
						<li>Concepts of Programming Languages.</li>
						<li>Data Communication and Networking</li>
						<li>Mathematics and Statistics (According to BSCS (DCS-UoK) courses)</li>
					</ul>
				</div>
			</div>
		</div>
	</main>

	<?php include 'footer.php'; ?>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<script>
		new WOW().init();
		$(document).ready(function() {
			$('.mdb-select').material_select();

			$(".sticky").sticky({
				topSpacing: 90
				, zIndex: 2
				, stopper: "#footer"
			});

			$('body').scrollspy({ offset: 100 });

			$(".smooth-scroll-custom").on("click",".smooth-scroll-link",function(t) {
				t.preventDefault();
				var e=$(this).attr("href");
				$("body,html").animate({
					scrollTop:$(e).offset().top - 90
				},700);
			});
		});
	</script>
</body>
</html>