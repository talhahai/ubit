function UpdateImageTable(Link, Table)
{
	$.ajax({
		type: "get",
		url: Link,
		async: false,
		success: function (ListResponse) {
			$("#"+Table).html("");
			var Random = Math.floor(Math.random()*1000);
			$.each(ListResponse, function(Key, Image) {
				var HTML = `<tr>
				<th class="th-image-priority">`+Image.Priority+`</th>
				<td>
				<div id="mdb-lightbox-ui"></div>
				<div class="mdb-lightbox no-margin">
				<figure>
				<a href="`+Image.ImageName+`?v=`+Random+`" data-size="`+Image.Width+`x`+Image.Height+`">
				<img src="`+Image.ThumbName+`?v=`+Random+`" class="img-fluid" />
				</a>
				</figure>
				</div>
				</td>
				<td>`+Image.Caption+`</td>
				<td class="pull-right">
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-vendor-image-priority" data-priority="up" data-id="`+Image.ID+`"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-vendor-image-priority" data-priority="down" data-id="`+Image.ID+`"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
				<a class="text-green h4 mr-1 ml-0 btn-edit-image" data-id="`+Image.ID+`" data-toggle="modal" data-target="#modal-edit-image"><i class="fa fa-pencil"></i></a>
				<a class="text-red h4 mr-1 ml-0 h4 mr-1 ml-0 btn-delete-image" data-id="`+Image.ID+`"><i class="fa fa-times" aria-hidden="true"></i></a>
				</td>
				</tr>`;
				$("#"+Table).append(HTML);
			});
		}
	});
}

function UpdatePartitionSection(Link, Section)
{
	$.ajax({
		type: "get",
		url: Link,
		async: false,
		success: function (ListResponse) {
			$("#"+Section).html("");
			$.each(ListResponse, function(Key, Partition) {
				var HTML = `<hr>
				<div class="row">
				<div class="col-12 col-sm-8">
				<p class="my-0 text-light-sea-green"><strong>`+Partition.Name+`</strong></p>
				<p class="my-0"><strong>Rent: Rs `+Partition.Rent+`</strong></p>
				<p class="my-0 text-light-sea-green label-discount"><b>`+Partition.Discount+`% OFF</b></p>
				<p class="my-0">Space: `+Partition.Space+`</p>
				<p class="my-0">Capacity: `+Partition.MinCapacity+` - `+Partition.MaxCapacity+`</p>
				<p class="text-muted"><small>`+Partition.Description+`</small></p>
				`+(Partition.Visible 
					? `<a class="text-muted h4 mr-1 ml-0 btn-partition-visible" data-id="`+Partition.ID+`"><i class="fa fa-eye" aria-hidden="true"></i></a>`
					: `<a class="text-muted h4 mr-1 ml-0 btn-partition-visible" data-id="`+Partition.ID+`"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>`)+`
				`+(Partition.Popular 
					? `<a class="text-red h4 mr-1 ml-0 btn-partition-popular" data-id="`+Partition.ID+`"><i class="fa fa-heart" aria-hidden="true"></i></a>`
					: `<a class="text-red h4 mr-1 ml-0 btn-partition-popular" data-id="`+Partition.ID+`"><i class="fa fa-heart-o" aria-hidden="true"></i></a>`)+`
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-partition-priority" data-priority="up" data-id="`+Partition.ID+`"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-partition-priority" data-priority="down" data-id="`+Partition.ID+`"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
				<a class="text-green h4 ml-1" href="/admin/venues/`+Partition.VenueID+`/partitions/`+Partition.ID+`"><i class="fa fa-pencil"></i></a>
				<a class="text-red h4 mr-1 ml-0 h4 mr-1 ml-0 btn-delete-partition" data-id="`+Partition.ID+`"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="col-12 col-sm-4">`;

				HTML += Lightbox(Partition.images);

				HTML += `</div>
				</div>`;
				$("#"+Section).append(HTML);
			});
		}
	});
}

function UpdateAddonSection(Link, Section)
{
	$.ajax({
		type: "get",
		url: Link,
		async: false,
		success: function (ListResponse) {
			$("#"+Section).html("");
			$.each(ListResponse, function(Key, Addon) {
				var HTML = `<div class="col-6 col-md-4 col-lg-3 py-3">
				<div class="card card-list collection-card">
				<div class="view overlay hm-zoom hm-white-slight card-image">
				<img src="`+Addon.ImageName+`" class="img-fluid" alt="`+Addon.Name+`">
				<div class="mask"></div>
				<div class="stripe light stripe-card-list p-2">
				<a>
				<p>Rs. `+Addon.Rent+`</p>
				</a>
				</div>
				</div>
				<div class="p-2">
				<h5 class="card-title my-2 text-light-sea-green">`+Addon.Name+`<small class="pull-right text-light-sea-green label-discount"><b>`+Addon.Discount+`% OFF</b></small></h5>
				<p class="card-subtitle my-1 text-muted"><small>`+Addon.Description+`</small></p>
				<div class="text-center mt-1">
				`+(Addon.Visible 
					? `<a class="text-muted h4 mr-1 ml-0 btn-addon-visible" data-id="`+Addon.ID+`"><i class="fa fa-eye" aria-hidden="true"></i></a>`
					: `<a class="text-muted h4 mr-1 ml-0 btn-addon-visible" data-id="`+Addon.ID+`"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>`)+`
				`+(Addon.Popular 
					? `<a class="text-red h4 mr-1 ml-0 btn-addon-popular" data-id="`+Addon.ID+`"><i class="fa fa-heart" aria-hidden="true"></i></a>`
					: `<a class="text-red h4 mr-1 ml-0 btn-addon-popular" data-id="`+Addon.ID+`"><i class="fa fa-heart-o" aria-hidden="true"></i></a>`)+`
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-addon-priority" data-priority="up" data-id="`+Addon.ID+`"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-addon-priority" data-priority="down" data-id="`+Addon.ID+`"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
				<a class="text-green h4 mr-1 ml-0 btn-edit-addon" data-id="`+Addon.ID+`" data-toggle="modal" data-target="#modal-edit-addon"><i class="fa fa-pencil"></i></a>
				<a class="text-red h4 mr-1 ml-0 h4 mr-1 ml-0 btn-delete-addon" data-id="`+Addon.ID+`"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
				</div>
				</div>
				</div>`;
				$("#"+Section).append(HTML);
			});
		}
	});
}

function UpdateMenuSection(Link, Section)
{
	$.ajax({
		type: "get",
		url: Link,
		async: false,
		success: function (ListResponse) {
			$("#"+Section).html("");
			$.each(ListResponse, function(Key, Menu) {
				var HTML = `<hr>
				<div class="row">
				<div class="col-12 col-sm-8">
				<div class="row">
				<div class="col-12 col-sm-6">
				<p class="my-0 text-light-sea-green"><strong>`+Menu.Name+`</strong></p>`;
				$.each(Menu.items, function(Key, Item) {
					var ItemChoices = [];
					$.each(Item.choices, function(Key, Choice) {
						ItemChoices.push(Choice.DishName);
					});
					HTML += `<p class="my-0">`+ItemChoices.join(' / ')+`</p>`;
				});
				HTML += `<div class="mt-1">
				`+(Menu.Visible 
					? `<a class="text-muted h4 mr-1 ml-0 btn-menu-visible" data-id="`+Menu.ID+`"><i class="fa fa-eye" aria-hidden="true"></i></a>`
					: `<a class="text-muted h4 mr-1 ml-0 btn-menu-visible" data-id="`+Menu.ID+`"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>`)+`
				`+(Menu.Popular 
					? `<a class="text-red h4 mr-1 ml-0 btn-menu-popular" data-id="`+Menu.ID+`"><i class="fa fa-heart" aria-hidden="true"></i></a>`
					: `<a class="text-red h4 mr-1 ml-0 btn-menu-popular" data-id="`+Menu.ID+`"><i class="fa fa-heart-o" aria-hidden="true"></i></a>`)+`
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-menu-priority" data-priority="up" data-id="`+Menu.ID+`"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-menu-priority" data-priority="down" data-id="`+Menu.ID+`"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
				<a class="text-green h4 ml-1" href="/admin/caterers/`+Menu.CatererID+`/menus/`+Menu.ID+`"><i class="fa fa-pencil"></i></a>
				<a class="text-red h4 mr-1 ml-0 h4 mr-1 ml-0 btn-delete-menu" data-id="`+Menu.ID+`"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
				</div>
				<div class="col-12 col-sm-6 text-right">
				<p class="my-0"><strong>Perhead: Rs `+Menu.Perhead+`</strong></p>
				<p class="my-0 text-light-sea-green label-discount"><b>`+Menu.Discount+`% OFF</b></p>
				<p class="my-0 text-muted"><strong>Suitable For `+Menu.Theme+`</strong></p>
				<p class="my-0">Serves: <strong>`+Menu.MinPerson+` - `+Menu.MaxPerson+` persons</strong></p>
				</div>
				</div>
				</div>
				<div class="col-12 col-sm-4">`;

				HTML += Lightbox(Menu.images);

				HTML += `</div>
				</div>`;
				$("#"+Section).append(HTML);
			});
		}
	});
}

function UpdateDishSection(Link, Section)
{
	$.ajax({
		type: "get",
		url: Link,
		async: false,
		success: function (ListResponse) {
			$("#"+Section).html("");
			$.each(ListResponse, function(Key, Dish) {
				var HTML = `<hr>
				<div class="row flex-center flex-center-auto-height">
				<div class="col-12 col-sm-8">
				<p class="my-0">
				<strong class="text-light-sea-green">`+Dish.Name+`</strong> 
				<strong>
				Rs `+Dish.PricePerUnit+`/`+Dish.Unit+` 
				<span class="text-light-sea-green label-discount">(`+Dish.Discount+`% OFF)</span>
				</strong> 
				(`+Dish.MinQuantity+` `+Dish.Unit+` - `+Dish.MaxQuantity+` `+Dish.Unit+`)
				</p>
				</div>
				<div class="col-4 text-right">
				`+(Dish.Visible 
					? `<a class="text-muted h4 mr-1 ml-0 btn-dish-visible" data-id="`+Dish.ID+`"><i class="fa fa-eye" aria-hidden="true"></i></a>`
					: `<a class="text-muted h4 mr-1 ml-0 btn-dish-visible" data-id="`+Dish.ID+`"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>`)+`
				`+(Dish.Popular 
					? `<a class="text-red h4 mr-1 ml-0 btn-dish-popular" data-id="`+Dish.ID+`"><i class="fa fa-heart" aria-hidden="true"></i></a>`
					: `<a class="text-red h4 mr-1 ml-0 btn-dish-popular" data-id="`+Dish.ID+`"><i class="fa fa-heart-o" aria-hidden="true"></i></a>`)+`
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-dish-priority" data-priority="up" data-id="`+Dish.ID+`"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-dish-priority" data-priority="down" data-id="`+Dish.ID+`"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
				<a class="text-green h4 mr-1 ml-0 btn-edit-dish" data-id="`+Dish.ID+`" data-toggle="modal" data-target="#modal-edit-dish"><i class="fa fa-pencil"></i></a>
				<a class="text-red h4 mr-1 ml-0 h4 mr-1 ml-0 btn-delete-dish" data-id="`+Dish.ID+`"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
				</div>`;
				$("#"+Section).append(HTML);
			});
		}
	});
}

function UpdateThemeSection(Link, Section)
{
	$.ajax({
		type: "get",
		url: Link,
		async: false,
		success: function (ListResponse) {
			$("#"+Section).html("");
			$.each(ListResponse, function(Key, Theme) {
				var HTML = `<hr>
				<div class="row">
				<div class="col-12 col-sm-8">
				<div class="row">
				<div class="col-12 col-sm-6">
				<p class="my-0 text-light-sea-green"><strong>`+Theme.Name+`</strong></p>
				<p class="my-0">Guests: `+Theme.MinPerson+` - `+Theme.MaxPerson+`</p>
				</div>
				<div class="col-12 col-sm-6 text-right">
				<p class="my-0"><strong>Perhead: Rs `+Theme.Perhead+`</strong></p>
				<p class="my-0 text-light-sea-green label-discount"><strong>`+Theme.Discount+`% OFF</strong></p>
				<p class="my-0 text-muted"><strong>Suitable For `+Theme.Theme+`</strong></p>
				</div>
				</div>
				<p class="text-muted"><small>`+Theme.Description+`</small></p>
				`+(Theme.Visible 
					? `<a class="text-muted h4 mr-1 ml-0 btn-theme-visible" data-id="`+Theme.ID+`"><i class="fa fa-eye" aria-hidden="true"></i></a>`
					: `<a class="text-muted h4 mr-1 ml-0 btn-theme-visible" data-id="`+Theme.ID+`"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>`)+`
				`+(Theme.Popular 
					? `<a class="text-red h4 mr-1 ml-0 btn-theme-popular" data-id="`+Theme.ID+`"><i class="fa fa-heart" aria-hidden="true"></i></a>`
					: `<a class="text-red h4 mr-1 ml-0 btn-theme-popular" data-id="`+Theme.ID+`"><i class="fa fa-heart-o" aria-hidden="true"></i></a>`)+`
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-theme-priority" data-priority="up" data-id="`+Theme.ID+`"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-theme-priority" data-priority="down" data-id="`+Theme.ID+`"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
				<a class="text-green h4 mr-1" href="/admin/decorators/`+Theme.DecoratorID+`/themes/`+Theme.ID+`"><i class="fa fa-pencil"></i></a>
				<a class="text-red h4 mr-1 ml-0 btn-delete-theme" data-id="`+Theme.ID+`"><i class="fa fa-times" aria-hidden="true"></i></a>

				</div>
				<div class="col-12 col-sm-4">`;

				HTML += Lightbox(Theme.images);

				HTML += `</div>
				</div>`;
				$("#"+Section).append(HTML);
			});
		}
	});
}

function UpdateServiceSection(Link, Section)
{
	$.ajax({
		type: "get",
		url: Link,
		async: false,
		success: function (ListResponse) {
			$("#"+Section).html("");
			$.each(ListResponse, function(Key, Service) {
				var HTML = `<div class="col-6 col-md-4 col-lg-3 py-3">
				<div class="card card-list collection-card">
				<div class="view overlay hm-zoom hm-white-slight card-image">
				<img src="`+Service.ImageName+`" class="img-fluid" alt="`+Service.Name+`">
				<div class="mask"></div>
				<div class="stripe light stripe-card-list p-2">
				<a>
				<p>Rs. `+Service.Rent+`</p>
				</a>
				</div>
				</div>
				<div class="p-2">
				<h5 class="card-title my-2 text-light-sea-green">`+Service.Name+`<small class="pull-right text-light-sea-green label-discount"><strong>`+Service.Discount+`% OFF</strong></small></h5>
				<p class="card-subtitle my-1 text-muted"><small>`+Service.Description+`</small></p>
				<div class="text-center mt-1">
				`+(Service.Visible 
					? `<a class="text-muted h4 mr-1 ml-0 btn-service-visible" data-id="`+Service.ID+`"><i class="fa fa-eye" aria-hidden="true"></i></a>`
					: `<a class="text-muted h4 mr-1 ml-0 btn-service-visible" data-id="`+Service.ID+`"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>`)+`
				`+(Service.Popular 
					? `<a class="text-red h4 mr-1 ml-0 btn-service-popular" data-id="`+Service.ID+`"><i class="fa fa-heart" aria-hidden="true"></i></a>`
					: `<a class="text-red h4 mr-1 ml-0 btn-service-popular" data-id="`+Service.ID+`"><i class="fa fa-heart-o" aria-hidden="true"></i></a>`)+`
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-service-priority" data-priority="up" data-id="`+Service.ID+`"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
				<a class="text-light-sea-green h4 mr-1 ml-0 btn-service-priority" data-priority="down" data-id="`+Service.ID+`"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
				<a class="text-green h4 mr-1 ml-0 btn-edit-service" data-id="`+Service.ID+`" data-toggle="modal" data-target="#modal-edit-service"><i class="fa fa-pencil"></i></a>
				<a class="text-red h4 mr-1 ml-0 btn-delete-service" data-id="`+Service.ID+`"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
				</div>
				</div>
				</div>`;
				$("#"+Section).append(HTML);
			});
		}
	});
}

function Lightbox(Images)
{
	var HTML = `<div class="mdb-lightbox-ui"></div>

	<!--Lightbox-->
	<div class="mdb-lightbox no-margin">`;
	$.each(Images, function(Key, Image) {
		if (Key == 0)
			HTML += `<figure class="col-md-8 col-12">`;
		else if (Key == 1 || Key == 2)
			HTML += `<figure class="col-md-4 col-6">`;
		else
			HTML += `<figure class="col-md-4 col-6 hidden-xl-down">`;
		HTML += `<a href="`+Image.ImageName+`" data-size="`+Image.Width+`x`+Image.Height+`">
		<img src="`+Image.ThumbName+`" class="img-fluid" />
		</a>`;
		if (Key == 2 && Images.length > 3)
			HTML += `<h3 class="lightbox-other-text flex-center m-0 text-white">
		+`+(Images.length - 3)+`
		</h3>`;
		HTML += `</figure>`;
	});
	HTML += `</div>
	<!--/.Lightbox-->`;

	return HTML;
}